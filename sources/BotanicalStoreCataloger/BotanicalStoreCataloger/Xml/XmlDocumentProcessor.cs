﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Linq;

using BotanicalStoreCataloger.Serialazable;
using BotanicalStoreCataloger.Xml.Abstract;

namespace BotanicalStoreCataloger.Xml
{
    /// <summary>
    /// Класс, реализующий работу с Xml-документами и каталогом растений.
    /// </summary>
    public class XmlDocumentProcessor : IDocumentProcessor<BotanicalCatalogue, XDocument>
    {
        #region Fields

        /// <summary>
        /// Генератор Xml-документов.
        /// </summary>
        public IDocumentGenerator<BotanicalCatalogue, XDocument> Generator { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий сериализацию дерева классов каталога в указанный файл.
        /// </summary>
        public void Serialize(string filePath, BotanicalCatalogue serializableObject)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BotanicalCatalogue));

            using (FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                serializer.Serialize(stream, serializableObject);
            }
        }

        /// <summary>
        /// Метод, реализующий десериализацию файла в дерево классов.
        /// </summary>
        public BotanicalCatalogue Deserialize(string filePath)
        {
            object deserializedObject = null;

            XmlSerializer serializer = new XmlSerializer(typeof(BotanicalCatalogue));

            using (FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                deserializedObject = serializer.Deserialize(stream);
            }

            return deserializedObject as BotanicalCatalogue;
        }

        /// <summary>
        /// Метод, реализующий сохранение Xml-документа в выходной файл.
        /// </summary>
        public void Save(XDocument document, string filePath)
        {
            document.Save(filePath);
        }

        #endregion

    }
}
