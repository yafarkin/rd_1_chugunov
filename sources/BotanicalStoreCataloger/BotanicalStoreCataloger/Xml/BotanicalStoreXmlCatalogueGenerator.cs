﻿using System;
using System.Linq;
using System.Xml.Linq;

using BotanicalStoreCataloger.Serialazable;
using BotanicalStoreCataloger.Xml.Abstract;

namespace BotanicalStoreCataloger.Xml
{
    public class BotanicalStoreXmlCatalogueGenerator : IDocumentGenerator<BotanicalCatalogue, XDocument>
    {
        #region Methods

        /// <summary>
        /// Метод, реализующий генерацию Xml-документа на основе получаемого каталога.
        /// </summary>
        /// <param name="catalogue">Каталог растений.</param>
        public XDocument Generate(BotanicalCatalogue catalogue)
        {
            XDocument document = new XDocument();

            var content = new XElement("CATALOG",
                             catalogue.Plants.Select(p =>
                                 new XElement("PLANT",
                                     new XElement("COMMON", new XAttribute("isPoison", p.Common.IsPoison), p.Common.Name),
                                     new XElement("BOTANICAL", p.BotanicalName),
                                     new XElement("ZONE", p.Zone),
                                     new XElement("LIGHT", p.Light),
                                     new XElement("PRICE", new XAttribute("dimension", p.Price.Dimension.ToString().ToLower()), p.Price.Value),
                                     new XElement("AVAILABILITY", p.Stock))));

            document.Add(content);

            return document;
        }

        #endregion
    }
}
