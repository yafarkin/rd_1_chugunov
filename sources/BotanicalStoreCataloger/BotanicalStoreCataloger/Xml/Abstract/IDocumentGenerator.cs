﻿using System;
using System.Xml.Linq;

namespace BotanicalStoreCataloger.Xml.Abstract
{
    /// <summary>
    /// Интерфейс, абстрактно представляющий некоторый генератор документов.
    /// </summary>
    /// <typeparam name="TIn">Тип входных данных.</typeparam>
    /// <typeparam name="TOut">Тип выходного документа.</typeparam>
    public interface IDocumentGenerator<TIn, TOut>
    {
        /// <summary>
        /// Метод, реализующий генерацию документа из некоторых входных данных.
        /// </summary>

        TOut Generate(TIn content);
    }
}
