﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotanicalStoreCataloger.Xml.Abstract
{
    /// <summary>
    /// Интерфейс, абстрактно представляющий некоторый препроцессор документов.
    /// </summary>
    /// <typeparam name="TIn">Тип входных данных.</typeparam>
    /// <typeparam name="TOut">Тип выходного документа.</typeparam>
    public interface IDocumentProcessor<TIn, TOut>
    {
        /// <summary>
        /// Генератор, реализующий генерацию документов.
        /// </summary>
        IDocumentGenerator<TIn, TOut> Generator { get; set; }

        /// <summary>
        /// Метод, реализующий сериализацию дерева классов в выходной файл.
        /// </summary>
        /// <param name="filePath">Путь к выходному файлу.</param>
        /// <param name="serializableObject">Дерево классов для сериализации.</param>
        void Serialize(string filePath, TIn serializableObject);

        /// <summary>
        /// Метод, реализующий десериализацию файла в дерево классов.
        /// </summary>
        /// <param name="filePath">Путь к файлу для десериализации.</param>
        TIn Deserialize(string filePath);

        /// <summary>
        /// Метод, реализующий сохранение документа в выходной файл.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="filePath"></param>
        void Save(TOut document, string filePath);


    }
}
