﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotanicalStoreCataloger.Configuration
{
    /// <summary>
    /// Класс, обеспечивающий связывание с конечными данными, 
    /// определенными в конфигурационном файле.
    /// </summary>
    public class XmlSerializationConfigElement : ConfigurationElement
    {
        /// <summary>
        /// Свойство, разворачиваемое в Xml-атрибут "fileType".
        /// </summary>
        [ConfigurationProperty("fileType", DefaultValue="", IsKey=true, IsRequired=true)]
        public string FileType
        {
            get
            {
                return (string) base["fileType"];
            }
            set
            {
                base["fileType"] = value;
            }
        }

        /// <summary>
        /// Свойство, разворачиваемое в Xml-атрибут "filePath".
        /// </summary>
        [ConfigurationProperty("path", DefaultValue = "", IsKey=false, IsRequired = false)]
        public string FilePath
        {
            get
            {
                return (string) base["path"];
            }
            set
            {
                base["path"] = value;
            }
        }
    }
}
