﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotanicalStoreCataloger.Configuration
{
    /// <summary>
    /// Класс, обеспечивающий взаимодействие с коллекцией элементов, 
    /// определенных в пользовательской секции app.config.
    /// </summary>
    [ConfigurationCollection(typeof(XmlSerializationConfigElement))]
    public class XmlFilesCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Метод, реализующий создание нового элемента коллекции.
        /// </summary>
        protected override ConfigurationElement CreateNewElement()
        {
            return new XmlSerializationConfigElement();
        }

        /// <summary>
        /// Метод, реализующий получение ключа элемента.
        /// </summary>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((XmlSerializationConfigElement) element).FileType;
        }

        /// <summary>
        /// Индексатор для обращения к элементам коллекции по индексу.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public XmlSerializationConfigElement this[int index]
        {
            get
            {
                return (XmlSerializationConfigElement) BaseGet(index);
            }
        }
    }
}
