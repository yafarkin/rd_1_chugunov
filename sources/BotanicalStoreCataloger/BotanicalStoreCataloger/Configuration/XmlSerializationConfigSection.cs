﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;

namespace BotanicalStoreCataloger.Configuration
{
    /// <summary>
    /// Вспомогательный класс для организации взаимодействия с пользовательской секцией в конфигурационном файле
    /// посредством ConfigurationManager.
    /// </summary>
    public class XmlSerializationConfigSection : ConfigurationSection
    {
        /// <summary>
        /// Свойство, разворачиваемое в Xml-элемент "Files".
        /// </summary>
        [ConfigurationProperty("Files")]
        public XmlFilesCollection Files
        {
            get
            {
                return (XmlFilesCollection) base["Files"];
            }
        }
    }
}
