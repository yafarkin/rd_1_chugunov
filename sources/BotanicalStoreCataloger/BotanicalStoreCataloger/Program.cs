﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using BotanicalStoreCataloger.Serialazable;
using BotanicalStoreCataloger.Configuration;
using BotanicalStoreCataloger.Xml;
using BotanicalStoreCataloger.Xml.Abstract;
using System.Configuration;
using System.Collections;

namespace BotanicalStoreCataloger
{
    public class Program
    {
        static void Main(string[] args)
        {
            #region Data initializing

            var plants = new Plant[]
            {
                new Plant
                {
                    Common = new CommonInfo { Name = "Cowslip", IsPoison = true },
                    BotanicalName = "Caltha palustris",
                    Zone = "4",
                    Light = "Mostly Shady",
                    Price = new Price { Dimension = PriceDimension.Dollars, Value = 9.90m },
                    Stock = 10
                },
                new Plant
                {
                    Common = new CommonInfo { Name = "Dutchman's-Breeches", IsPoison = false },
                    BotanicalName = "Dicentra cucullaria",
                    Zone = "3",
                    Light = "Mostly Shady",
                    Price = new Price { Dimension = PriceDimension.Dollars, Value = 6.44m },
                    Stock = 234
                },
                new Plant
                {
                    Common = new CommonInfo { Name = "Ginger, Wild", IsPoison = true },
                    BotanicalName = "Asarum canadense",
                    Zone = "4",
                    Light = "Mostly Shady",
                    Price = new Price { Dimension = PriceDimension.Euros, Value = 9.03m },
                    Stock = 3
                },
                new Plant
                {
                    Common = new CommonInfo { Name = "Hepatica", IsPoison = false },
                    BotanicalName = "Hepatica americana",
                    Zone = "4",
                    Light = "Mostly Shady",
                    Price = new Price { Dimension = PriceDimension.Dollars, Value = 4.45m },
                    Stock = 56
                },
                new Plant
                {
                    Common = new CommonInfo { Name = "Liverleaf", IsPoison = true },
                    BotanicalName = "Hepatica americana",
                    Zone = "4",
                    Light = "Mostly Shady",
                    Price = new Price { Dimension = PriceDimension.Euros, Value = 3.99m },
                    Stock = 37
                }
            };

            #endregion

            var catalogue = new BotanicalCatalogue { Plants = plants };

            IDocumentGenerator<BotanicalCatalogue, XDocument> generator = new BotanicalStoreXmlCatalogueGenerator();
            IDocumentProcessor<BotanicalCatalogue, XDocument> processor = new XmlDocumentProcessor { Generator = generator };

            var filePath = "catalogue.xml";

            processor.Save(processor.Generator.Generate(catalogue), filePath);

            Console.WriteLine($"File saved to { filePath }");

            filePath = ((XmlSerializationConfigSection)ConfigurationManager.GetSection("XmlSerializationConfigSection")).Files[0].FilePath;

            processor.Serialize(filePath, catalogue);  
            Console.WriteLine($"Class Tree serialized to file { filePath }");

            var deserializedTree = processor.Deserialize(filePath);
            Console.WriteLine($"Class Tree deserialized from { filePath }");

            filePath = "deserializedCatalogue.xml";

            processor.Save(processor.Generator.Generate(deserializedTree), filePath);
            Console.WriteLine($"Deserialized class tree saved to { filePath }", filePath);

            Console.ReadLine();

        }
    }
}
