﻿using System;
using System.Xml.Serialization;

namespace BotanicalStoreCataloger.Serialazable
{
    /// <summary>
    /// Класс, предоставляющий общие сведения о растении.
    /// </summary>
    [Serializable]
    public class CommonInfo
    {
        /// <summary>
        /// Общее название растения.
        /// </summary>
        [XmlText()]
        public string Name { get; set; }

        /// <summary>
        /// Ядовитость растения.
        /// </summary>
        [XmlAttribute(AttributeName = "isPoison", DataType = "boolean")]
        public bool IsPoison { get; set; }
    }
}
