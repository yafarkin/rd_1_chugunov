﻿using System;
using System.Xml.Serialization;

namespace BotanicalStoreCataloger.Serialazable
{
    /// <summary>
    /// Класс, реализующий сущность "Стоимость".
    /// Сущность сериализуема.
    /// </summary>
    [Serializable]
    public class Price
    {
        /// <summary>
        /// Размер стоимости.
        /// </summary>
        [XmlText()]
        public decimal Value { get; set; }

        /// <summary>
        /// Валюта, в которой измеряется стоимость.
        /// </summary>
        [XmlAttribute(AttributeName = "dimension")]
        public PriceDimension Dimension { get; set; }
    }
}
