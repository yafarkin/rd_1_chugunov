﻿using System;
using System.Xml.Serialization;

namespace BotanicalStoreCataloger.Serialazable
{
    /// <summary>
    /// Класс, реализующий сущность "Растение".
    /// Сущность сериализуема.
    /// </summary>
    [Serializable]
    public class Plant
    {
        /// <summary>
        /// Общие сведения о растении.
        /// </summary>
        [XmlElement(ElementName = "COMMON")]
        public CommonInfo Common { get; set; }

        /// <summary>
        /// Ботаническое название растения.
        /// </summary>
        [XmlElement(ElementName = "BOTANICAL")]
        public string BotanicalName { get; set; }

        /// <summary>
        /// Зона морозостойкости растения.
        /// </summary>
        [XmlElement(ElementName = "ZONE")]
        public string Zone { get; set; }

        /// <summary>
        /// Необходимое количество солнечного света для растения.
        /// </summary>
        [XmlElement(ElementName = "LIGHT")]
        public string Light { get; set; }

        /// <summary>
        /// Стоимость растения.
        /// </summary>
        [XmlElement(ElementName = "PRICE")]
        public Price Price { get; set; }

        /// <summary>
        /// Количество саженцев растения на остатке.
        /// </summary>
        [XmlElement(ElementName = "AVAILABILITY")]
        public int Stock { get; set; }
    }
}
