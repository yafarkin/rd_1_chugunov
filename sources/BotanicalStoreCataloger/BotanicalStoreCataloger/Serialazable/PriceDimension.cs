﻿using System;
using System.Xml.Serialization;

namespace BotanicalStoreCataloger.Serialazable
{
    /// <summary>
    /// Перечисление валют.
    /// </summary>
    public enum PriceDimension : byte
    {
        [XmlEnum(Name = "dollars")]
        Dollars = 1,

        [XmlEnum(Name = "euros")]
        Euros = 2,
    }
}
