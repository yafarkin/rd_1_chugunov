﻿using System;
using System.Xml.Serialization;

namespace BotanicalStoreCataloger.Serialazable
{
    /// <summary>
    /// Сущность, реализующая объект "Каталог".
    /// Сущность сериализуема в Xml, представляет собой корневой элемент Xml-документа.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "CATALOG")]
    public class BotanicalCatalogue
    {
        /// <summary>
        /// Перечень растений, составляющих каталог.
        /// </summary>
        [XmlElement(ElementName = "PLANT")]
        public Plant[] Plants { get; set; }
    }
}
