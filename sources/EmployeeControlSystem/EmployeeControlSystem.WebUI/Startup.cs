﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmployeeControlSystem.WebUI.Startup))]
namespace EmployeeControlSystem.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
