﻿using System;
using System.Web.Http;
using System.Web.Mvc;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

using EmployeeControlSystem.BLL.Services;
using EmployeeControlSystem.BLL.Services.Abstract;
using EmployeeControlSystem.DAL.Data;
using EmployeeControlSystem.DAL.Data.Abstract;
using EmployeeControlSystem.DAL.Components;
using EmployeeControlSystem.DAL.Components.Abstract;

namespace EmployeeControlSystem.WebUI
{
    // <summary>
    /// Класс, реализующий загрузчик контейнера Unity.
    /// </summary>
    public static class UnityBootstrapper
    {
        /// <summary>
        /// Метод, реализующий инициализацию контейнера Unity.
        /// </summary>
        public static IUnityContainer Initialize()
        {
            var container = BuildUnityContainer();

            // Назначение приложению созданного контейнера в качестве внедрителя зависимостей.
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
			GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

			return container;
        }

        /// <summary>
        /// Метод, реализующий регистрацию типов для контейнера Unity.
        /// Для каждого IInterface, Unity будет создавать сопоставленный ему Type.
        /// </summary>
        /// <param name="container">Контейнер, для которого осуществляется регистрация типов.</param>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IDataWarehouseContext, EFDataWarehouseContext>();
            container.RegisterType<IEmployeeRepository, EmployeeRepository>();
            container.RegisterType<IEmployeeService, EmployeeService>();
        }

        /// <summary>
        /// Метод, реализующий построение контейнера Unity.
        /// </summary>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            RegisterTypes(container);

            return container;
        }


    }
}