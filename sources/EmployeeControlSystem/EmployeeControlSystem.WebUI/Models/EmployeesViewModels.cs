﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EmployeeControlSystem.BLL.DTO;

namespace EmployeeControlSystem.WebUI.Models
{
	public class EmployeeListViewModel
	{
		public IEnumerable<EmployeeViewModel> Employees { get; set; }
	}

	public class EmployeeViewModel
	{
		[HiddenInput(DisplayValue=false)]
		public int Id { get; set; }

		/// <summary>
		/// Фамилия сотрудника.
		/// </summary>
		[Required(ErrorMessage = "Пожалуйста, укажите фамилию сотрудника.")]
		[Display(Name = "Фамилия")]
		[MaxLength(50, ErrorMessage = "Длина фамилии не должна превышать 50 символов.")]
		public string Lastname { get; set; }

		/// <summary>
		/// Имя сотрудника.
		/// </summary>
		[Required(ErrorMessage = "Пожалуйста, укажите имя сотрудника.")]
		[Display(Name = "Имя")]
		[MaxLength(50, ErrorMessage = "Длина имени не должна превышать 50 символов.")]
		public string Firstname { get; set; }

		/// <summary>
		/// Отчество сотрудника.
		/// </summary>
		[Required(ErrorMessage = "Пожалуйста, укажите отчество сотрудника.")]
		[Display(Name = "Отчество")]
		[MaxLength(50, ErrorMessage = "Длина отчества не должна превышать 50 символов!")]
		public string Patronymic { get; set; }

		/// <summary>
		/// Номер телефона сотрудника.
		/// </summary>
		[Required(ErrorMessage = "Пожалуйста, укажите номер телефона сотрудника.")]
		[Display(Name = "Номер телефона")]
		[MaxLength(12, ErrorMessage = "Длина номера телефона не должна превышать 12 символов!")]
		[RegularExpression(@"^((8|0|((\+|00)\d{1,2}))[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$")]
		public string Phone { get; set; }

		/// <summary>
		/// Email-адрес сотрудника.
		/// </summary>
		[Required(ErrorMessage = "Укажите E-mail сотрудника.")]
		[Display(Name = "E-mail")]
		[RegularExpression(@".+\@.+\..+", ErrorMessage = "Пожалуйста, укажите корректный e-mail.")]
		public string Email { get; set; }

		public int PositionId { get; set; }
	}
}