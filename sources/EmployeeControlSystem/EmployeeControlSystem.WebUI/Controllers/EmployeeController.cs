﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EmployeeControlSystem.BLL.DTO;
using EmployeeControlSystem.BLL.Services.Abstract;
using EmployeeControlSystem.WebUI.Models;

namespace EmployeeControlSystem.WebUI.Controllers
{
	public class EmployeeController : Controller
	{
		private IEmployeeService _employeeService;

		public EmployeeController(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		public ViewResult Index()
		{
			var model = new EmployeeListViewModel
			{
				Employees = _employeeService.GetAllEmployees()
					.Select(emp => new EmployeeViewModel
					{
						Id = emp.Id,
						Lastname = emp.Lastname,
						Firstname = emp.Firstname,
						Patronymic = emp.Patronymic,
						Phone = emp.Phone,
						Email = emp.Email,
						PositionId = emp.PositionId
					})
			};

			return View(model);
		}

		[HttpGet]
		public ActionResult Add()
		{
			var model = new EmployeeViewModel { Id = 0 };

			ViewBag.legend = "Создание нового сотрудника";
			return View(model);
		}

		[HttpGet]
		public ActionResult Edit(int id)
		{
			var employee = _employeeService.GetEmployeeById(id);

			var model = new EmployeeViewModel
			{
				Id = employee.Id,
				Lastname = employee.Lastname,
				Firstname = employee.Firstname,
				Patronymic = employee.Patronymic,
				Phone = employee.Phone,
				Email = employee.Email,
				PositionId = employee.PositionId
			};

			ViewBag.legend = "Редактирование данных сотрудника";

			return View("Add", model);
		}

		[HttpPost]
		public ActionResult Save(EmployeeViewModel employee)
		{
			if (ModelState.IsValid)
			{
				_employeeService.SaveEmployee(new EmployeeDto
				{
					Id = employee.Id,
					Lastname = employee.Lastname,
					Firstname = employee.Firstname,
					Patronymic = employee.Patronymic,
					Phone = employee.Phone,
					Email = employee.Email,
					PositionId = employee.PositionId
				});
				return RedirectToAction("Index");
			}
			else
			{
				return View("Add");
			}
		}

		[HttpGet]
		public ActionResult Remove(int id)
		{
			_employeeService.RemoveEmployee(id);

			return RedirectToAction("Index");
		}
	}
}