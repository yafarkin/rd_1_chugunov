﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace EmployeeControlSystem.DAL.Data.Abstract
{
    /// <summary>
    /// Описывает контекст хранилища данных.
    /// </summary>
    public interface IDataWarehouseContext : IDisposable
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity;

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }
}
