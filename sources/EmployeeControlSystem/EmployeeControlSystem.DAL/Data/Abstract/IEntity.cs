﻿using System;

namespace EmployeeControlSystem.DAL.Data.Abstract
{
    /// <summary>
    /// Описывает сущность.
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
