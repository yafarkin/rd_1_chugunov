﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using EmployeeControlSystem.DAL.Data.Abstract;
using EmployeeControlSystem.DAL.Data.Entity;

namespace EmployeeControlSystem.DAL.Data
{
    public class EFDataWarehouseContext : DbContext, IDataWarehouseContext
    {
        public EFDataWarehouseContext() : base("name=EmployeeControlSystem.DataWarehouse") { }

        /// <summary>
        /// Возвращает набор экземпляров сущностей заданного типа.
        /// </summary>
        /// <typeparam name="TEntity">Требуемая сущность-тип.</typeparam>
        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity
        {
            return base.Set<TEntity>();
        }

        /// <summary>
        /// Выполняет настройку модели данных при ее создании.
        /// </summary>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Employee>()
                .ToTable("Employees")
                .HasKey(e => e.Id)
                .HasRequired(e => e.Position)
                .WithMany(p => p.EmployeesInPosition)
                .HasForeignKey(e => e.PositionId);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Lastname)
                .IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.Firstname)
                .IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.Patronymic)
                .IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.Phone)
                .IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.Email)
                .IsRequired();

            modelBuilder.Entity<Position>()
                .ToTable("Positions")
                .HasKey(p => p.Id);
        }
    }
}
