﻿using System;
using System.Collections.Generic;

using EmployeeControlSystem.DAL.Data.Abstract;

namespace EmployeeControlSystem.DAL.Data.Entity
{
    /// <summary>
    /// Описывает сущность "Должность".
    /// </summary>
    public class Position : IEntity
    {
        /// <summary>
        /// Код должности.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название должности.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Перечень сотрудников, занимающих данную должность.
        /// </summary>
        public ICollection<Employee> EmployeesInPosition { get; set; }
    }
}
