﻿using System;
using System.Collections.Generic;

using EmployeeControlSystem.DAL.Data.Abstract;

namespace EmployeeControlSystem.DAL.Data.Entity
{
    /// <summary>
    /// Описывает сущность "Сотрудник".
    /// </summary>
    public class Employee : IEntity
    {
        /// <summary>
        /// Табельный номер сотрудника.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Фамилия сотрудника.
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Имя сотрудника.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Отчество сотрудника.
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// Номер телефона сотрудника.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Email-адрес сотрудника.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Код роли сотрудника.
        /// </summary>
        public int PositionId { get; set; }

        /// <summary>
        /// Должность сотрудника.
        /// </summary>
        public Position Position { get; set; }
    }
}
