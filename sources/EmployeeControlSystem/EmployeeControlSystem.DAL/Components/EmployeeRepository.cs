﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

using EmployeeControlSystem.DAL.Components.Abstract;
using EmployeeControlSystem.DAL.Data.Abstract;
using EmployeeControlSystem.DAL.Data.Entity;

namespace EmployeeControlSystem.DAL.Components
{
    public class EmployeeRepository : IEmployeeRepository
    {
        /// <summary>
        /// Экземпляр контекста хранилища данных.
        /// </summary>
        private readonly IDataWarehouseContext _dataWarehouseContext;

        private bool _disposed = false;

        public EmployeeRepository(IDataWarehouseContext dataWarehouseContext)
        {
            _dataWarehouseContext = dataWarehouseContext;
        }

        /// <summary>
        /// Реализует добавление нового сотрудника в хранилище.
        /// </summary>
        public void Add(Employee entity)
        {
            _dataWarehouseContext.Set<Employee>().Add(entity);

			Commit();
        }

        /// <summary>
        /// Реализует обновление данных сотрудника в хранилище.
        /// </summary>
        public void Update(Employee entity)
        {
            _dataWarehouseContext.Entry(entity).State = EntityState.Modified;

            Commit();
        }

        /// <summary>
        /// Реализует удаление заданного сотрудника.
        /// </summary>
        public void Remove(int id)
        {
            Employee employee = _dataWarehouseContext.Set<Employee>().Find(id);

            if (employee != null)
            {
                _dataWarehouseContext.Set<Employee>().Remove(employee);
            }

            Commit();


            Commit();
        }

        /// <summary>
        /// Реализует получение списка всех сотрудников.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Employee> All()
        {
            return _dataWarehouseContext.Set<Employee>().AsEnumerable();
        }

        /// <summary>
        /// Реализует получение сотрудника по его табельному номеру.
        /// </summary>
        public Employee GetById(int id)
        {
            return _dataWarehouseContext.Set<Employee>().Find(id);
        }

        /// <summary>
        /// Реализует сохранение изменений в хранилище.
        /// </summary>
        public void Commit()
        {
            try
            {
                _dataWarehouseContext.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Реализует получение списка сотрудников, удовлетворяющих заданному условию.
        /// </summary>
        /// <param name="condition">Условие отбора.</param>
        public IEnumerable<Employee> Find(Expression<Func<Employee, bool>> condition)
        {
            return _dataWarehouseContext.Set<Employee>().Where(condition).AsEnumerable();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dataWarehouseContext.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

    }
}
