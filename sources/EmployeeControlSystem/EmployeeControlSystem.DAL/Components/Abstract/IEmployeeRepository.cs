﻿using System;

using EmployeeControlSystem.DAL.Data.Entity;

namespace EmployeeControlSystem.DAL.Components.Abstract
{
    /// <summary>
	/// Описывает функционал, необходимый для работы с экземплярами сущности типа "Сотрудник".
	/// </summary>
    public interface IEmployeeRepository : IRepository<Employee> { }
}
