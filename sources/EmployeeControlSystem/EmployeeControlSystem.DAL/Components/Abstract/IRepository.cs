﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using EmployeeControlSystem.DAL.Data.Abstract;

namespace EmployeeControlSystem.DAL.Components.Abstract
{
    /// <summary>
	/// Описывает функционал, необходимый для работы с экземплярами сущностей любого типа.
	/// </summary>
	/// <typeparam name="TEntity">Необходимая сущность-тип.</typeparam>
    public interface IRepository<TEntity> : IDisposable where TEntity : IEntity
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(int id);

        IEnumerable<TEntity> All();
        TEntity GetById(int id);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> condition);

        void Commit();
    }
}
