﻿SET IDENTITY_INSERT dbo.Positions ON

DECLARE @Positions TABLE(
	[Id]		INT				NOT NULL,
	[Name]		NVARCHAR(50)	NOT NULL
	)

INSERT INTO @Positions ([Id], [Name]) VALUES
(1, N'Менеджер'),
(2, N'Секретарь'),
(3, N'Бухгалтер'),
(4, N'Системный администратор'),
(5, N'Разработчик'),
(6, N'Дизайнер'),
(7, N'Тестировщик')

MERGE INTO [Positions] AS [target]
USING @Positions AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Name] = [source].[Name]

WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Name]) 
	VALUES ([source].[Id], [source].[Name]);

SET IDENTITY_INSERT dbo.Positions OFF
GO