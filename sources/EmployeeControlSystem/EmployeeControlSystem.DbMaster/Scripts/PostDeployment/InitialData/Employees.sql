﻿SET IDENTITY_INSERT dbo.Employees ON

DECLARE @Employees TABLE(
	[Id]			INT				NOT NULL,
	[Lastname]		NVARCHAR(50)	NOT NULL,
	[Firstname]		NVARCHAR(50)	NOT NULL,
	[Patronymic]	NVARCHAR(50)	NOT NULL,
	[Phone]			NVARCHAR(16)	NOT NULL,
	[Email]			NVARCHAR(50)	NOT NULL,
	[PositionId]	INT				NOT NULL
	)

INSERT INTO @Employees ([Id], [Lastname], [Firstname], [Patronymic], [Phone], [Email], [PositionId]) VALUES
(1, N'Орлов', N'Игорь', N'Викторович', N'+793794857346', N'igor.orlov@gmail.com', 1),
(2, N'Орлова', N'Мария', N'Степановна', N'+79061284852', N'mariya.orlova@gmail.com', 2),
(3, N'Сидорчук', N'Ольга', N'Павловна', N'+79372165419', N'olga.sidorchuk@gmail.com', 3),
(4, N'Ветров', N'Владимир', N'Сергеевич', N'+79276153766', N'vladimir.vetrov@gmail.com', 4),
(5, N'Сидоров', N'Игорь', N'Степанович', N'+79875467312', N'igor.sidorov@gmail.com', 5),
(6, N'Абрасимов', N'Василий', N'Владленович', N'+79763776356', N'vasiliy.abrasimov@gmail.com', 5),
(7, N'Волоцуев', N'Евгений', N'Петрович', N'+79794857346', N'evgeny.volotsuev@gmail.com', 6),
(8, N'Кондратьев', N'Глеб', N'Викторович', N'+79682394834', N'kondratyev.gleb@gmail.com', 7)

MERGE INTO [Employees] AS [target]
USING @Employees AS [source] ON [target].[Id] = [source].[Id]
WHEN MATCHED THEN
	UPDATE
		SET	[target].[Lastname] = [source].[Lastname],
			[target].[Firstname] = [source].[Firstname],
			[target].[Patronymic] = [source].[Patronymic],
			[target].[Phone] = [source].[Phone],
			[target].[Email] = [source].[Email],
			[target].[PositionId] = [source].[PositionId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Id], [Lastname], [Firstname], [Patronymic], [Phone], [Email], [PositionId]) 
	VALUES ([source].[Id], [source].[Lastname], [source].[Firstname], [source].[Patronymic], [source].[Phone], [source].[Email], [source].[PositionId]);

SET IDENTITY_INSERT dbo.Employees OFF
GO