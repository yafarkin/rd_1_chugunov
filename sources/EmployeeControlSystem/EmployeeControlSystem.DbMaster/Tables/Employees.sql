﻿CREATE TABLE [dbo].[Employees]
(
	[Id] INT NOT NULL IDENTITY(1,1), 
    [Lastname] NVARCHAR(50) NOT NULL, 
    [Firstname] NVARCHAR(50) NOT NULL, 
    [Patronymic] NVARCHAR(50) NOT NULL, 
    [Phone] NVARCHAR(16) NOT NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [PositionId] INT NOT NULL

	CONSTRAINT [PK_Employees] PRIMARY KEY ([Id] ASC)
)
