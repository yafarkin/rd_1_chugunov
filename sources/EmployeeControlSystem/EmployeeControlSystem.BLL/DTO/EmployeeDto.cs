﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeControlSystem.BLL.DTO
{
    public class EmployeeDto
    {
        /// <summary>
        /// Табельный номер сотрудника.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Фамилия сотрудника.
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Имя сотрудника.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Отчество сотрудника.
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// Номер телефона сотрудника.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Email-адрес сотрудника.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Код роли сотрудника.
        /// </summary>
        public int PositionId { get; set; }

    }
}
