﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EmployeeControlSystem.BLL.Services.Abstract;
using EmployeeControlSystem.BLL.DTO;
using EmployeeControlSystem.DAL.Data.Entity;
using EmployeeControlSystem.DAL.Components.Abstract;

namespace EmployeeControlSystem.BLL.Services
{
	/// <summary>
	/// Реализует функционал для работы с данными сотрудников на уровне бизнес-логики.
	/// </summary>
	public class EmployeeService : IEmployeeService
	{
		private readonly IEmployeeRepository _employeeRepository;

		public EmployeeService(IEmployeeRepository employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		/// <summary>
		/// Реализует получение списка всех сотрудников.
		/// </summary>
		public IEnumerable<EmployeeDto> GetAllEmployees()
		{
			var outList = _employeeRepository.All()
				.Select(e => new EmployeeDto
				{
					Id = e.Id,
					Lastname = e.Lastname,
					Firstname = e.Firstname,
					Patronymic = e.Patronymic,
					Phone = e.Phone,
					Email = e.Email,
					PositionId = e.PositionId
				});

			return outList;
		}

		public EmployeeDto GetEmployeeById(int id)
		{
			var employee = _employeeRepository.GetById(id);

			return new EmployeeDto
			{
				Id = employee.Id,
				Lastname = employee.Lastname,
				Firstname = employee.Firstname,
				Patronymic = employee.Patronymic,
				Phone = employee.Phone,
				Email = employee.Email,
				PositionId = employee.PositionId
			};
		}

		public void SaveEmployee(EmployeeDto employee)
		{
			if (employee.Id == 0)
			{
				_employeeRepository.Add(new Employee
				{
					Id = employee.Id,
					Lastname = employee.Lastname,
					Firstname = employee.Firstname,
					Patronymic = employee.Patronymic,
					Phone = employee.Phone,
					Email = employee.Email,
					PositionId = employee.PositionId
				});
			}
			else
			{
				_employeeRepository.Update(new Employee
				{
					Id = employee.Id,
					Lastname = employee.Lastname,
					Firstname = employee.Firstname,
					Patronymic = employee.Patronymic,
					Phone = employee.Phone,
					Email = employee.Email,
					PositionId = employee.PositionId
				});
			}
		}

		/// <summary>
		/// Реализует добавление нового сотрудника.
		/// </summary>
		public void AddEmployee(EmployeeDto employee)
		{
			_employeeRepository.Add(new Employee
			{
				Id = employee.Id,
				Lastname = employee.Lastname,
				Firstname = employee.Firstname,
				Patronymic = employee.Patronymic,
				Phone = employee.Phone,
				Email = employee.Email,
				PositionId = employee.PositionId
			});
		}

		public void RemoveEmployee(int id)
		{
			_employeeRepository.Remove(id);
		}


	}
}
