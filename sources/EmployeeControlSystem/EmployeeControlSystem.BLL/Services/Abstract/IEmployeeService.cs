﻿using System;
using System.Collections.Generic;

using EmployeeControlSystem.BLL.DTO;

namespace EmployeeControlSystem.BLL.Services.Abstract
{
    /// <summary>
    /// Описывает функционал для работы с данными сотрудников на уровне бизнес-логики.
    /// </summary>
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDto> GetAllEmployees();

		EmployeeDto GetEmployeeById(int id);

		void AddEmployee(EmployeeDto employee);

        void SaveEmployee(EmployeeDto employee);

        void RemoveEmployee(int id);

    }
}
