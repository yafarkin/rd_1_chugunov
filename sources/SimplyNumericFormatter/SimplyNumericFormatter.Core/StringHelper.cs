﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SimplyNumericFormatter.Core
{
    /// <summary>
    /// Класс, реализующий вспомогательные методы расширения 
    /// для базового класса String по обработке строк.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Закрытый метод класса для установки корректного разделителя целой и дробной части числа.
        /// Разделитель варьируется в зависимости от текущей локали.
        /// </summary>
        /// <param name="stringNumber">Строковое представление числа.</param>
        /// <returns></returns>
        private static string SetCorrectDecimalSeparator(String stringNumber)
        {
            return CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == "." ?
                               stringNumber.Replace(',', '.') :
                               stringNumber.Replace('.', ',');
        }

        /// <summary>
        /// Метод, реализующий чисел из строки в виде десятичных чисел с плавающей запятой.
        /// </summary>
        /// <param name="line">Входная строка для обработки.</param>
        /// <returns>
        /// Возвращает массив десятичных чисел с плавающей запятой.
        /// </returns>
        public static Decimal[] ExtractNumbersAsDecimals(this String line)
        {
            List<Decimal> results = new List<Decimal>();

            String pattern = @"[+-]?\d+[.,]\d+";

            MatchCollection matches = Regex.Matches(line, pattern);

            foreach (Match match in matches)
            {
                results.Add(Convert.ToDecimal(SetCorrectDecimalSeparator(match.Value)));
            }

            return results.Count == 0 ? null : results.ToArray();

        }

        /// <summary>
        /// Метод, реализующий извлечение чисел из заданной строки в виде заданного типа.
        /// Метод приватный, т.к. реализация метода еще не завершена(имеются ошибки с переводом в целочисленные типы).
        /// </summary>
        /// <typeparam name="T">Тип результирующего набора значений.</typeparam>
        /// <param name="line">Строка, подлежащая анализу.</param>
        /// <returns></returns>
        private static T[] ExtractNumbersAs<T>(this string line)
        {
            List<T> results = new List<T>();

            String pattern = @"[+-]?\d+[.,]\d+";

            MatchCollection matches = Regex.Matches(line, pattern);

            foreach (Match match in matches)
            {
                results.Add((T)Convert.ChangeType(SetCorrectDecimalSeparator(match.Value), typeof(T)));
            }

            return results.ToArray();
        }
    }
}
