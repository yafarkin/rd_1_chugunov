﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using SimplyNumericFormatter.Core;

namespace TextfileParser.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            if (Console.In.Peek() != -1) 
            {
                var fullInput = new StringBuilder();
                var currentLine = Console.ReadLine();

                while (currentLine != null)
                {
                    fullInput.AppendLine(currentLine);     
                    currentLine = Console.ReadLine();
                }

                txtData.AppendText(fullInput.ToString());
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void menuSubItemFileOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Текстовые файлы(.txt)|*.txt" + "|Все файлы (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                lblFileName.Content = openFileDialog.FileName;
                txtData.Clear();

                using (StreamReader reader = new StreamReader(openFileDialog.FileName))
                {
                    txtData.AppendText(reader.ReadToEnd());
                }

                btnFormat.IsEnabled = true;
                btnClear.IsEnabled = true;
            }
        }

        private void btnFormat_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Clear();

            if (!string.IsNullOrEmpty(txtData.Text))
            {
                List<String> lines = new List<String>();

                for (int i = 0; i < txtData.LineCount; i++)
                {
                    lines.Add(txtData.GetLineText(i));
                }

                bool hasNoResults = true;

                foreach (String line in lines)
                {
                    Decimal[] results = line.ExtractNumbersAsDecimals();

                    if (results != null)
                    {
                        hasNoResults = false;

                        AlphabeticSymbol label = AlphabeticSymbol.A;

                        StringBuilder builder = new StringBuilder();

                        foreach (var num in results)
                        {
                            builder.AppendFormat("{0}:{1} ", label, num);

                            label = label == AlphabeticSymbol.Z ? AlphabeticSymbol.A : ++label;
                        }
                        txtResult.AppendText(builder.ToString());
                        txtResult.AppendText(Environment.NewLine);
                    }
                }

                if (hasNoResults)
                {
                    txtResult.AppendText("Валидные числа для форматирования не обнаружены!");
                }
            }
        }

        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите завершить работу с приложением?", "Подтвреждение выхода", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtData.Text))
            {
                if (MessageBox.Show("Все несохраненные результаты будут удалены! Вы действительно хотите продолжить?", "Подтвреждение очистки", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    txtData.Clear();
                    txtResult.Clear();

                    lblFileName.Content = string.Empty;
                }
            }
        }
    }
}
