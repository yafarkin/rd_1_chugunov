﻿using System;
using System.Collections.Generic;
using SimplyNumericFormatter.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimplyNumericFormatter.UnitTests
{
    /// <summary>
    /// Тестовый класс, инкапсулирующий методы тестирования функциональности, 
    /// реализуемой классом StringHelper.
    /// </summary>
    [TestClass]
    public class StringHelperTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности возвращаемых значений, отобранных из строки.
        /// </summary>
        [TestMethod]
        public void StringHelperExtractsDecimalsFromSingleLineCorrect()
        {
            Decimal[] expectedExtractingResult = { 1.23m, 3.45m, 5.56m };
            String incomingLine = "1.23,3.45,5.56";

            Decimal[] actualExtractingResult = incomingLine.ExtractNumbersAsDecimals();

            CollectionAssert.AreEqual(expectedExtractingResult, actualExtractingResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректности возвращаемых значений, отобранных из множества строк..
        /// </summary>
        [TestMethod]
        public void StringHelperExtractsDecimalsFromMultipleLineCorrect()
        {
            var expectedExtractingResult = new List<Decimal[]>
            {
                new Decimal[] { 1.23m, 3.45m, 5.56m },
                new Decimal[] { 3.45m, 5.67m, 7.89m }
            };
                                                          
            String[] incomingLines = {
                                       "1.23,3.45,5.56",
                                       "3.45,5.67,7.89"
                                     };

            var actualExtractingResult = new List<Decimal[]>();

            foreach (var line in incomingLines)
            {
                actualExtractingResult.Add(line.ExtractNumbersAsDecimals());
            }

            CollectionAssert.AreEqual(expectedExtractingResult, actualExtractingResult);
        }
    }
}
