﻿using System;
using System.Text;
using System.Collections.Generic;
using SimplyNumericFormatter.Core;

namespace SimplyNumericFormatter.CUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            #region Data input

            List<String> fileContents = new List<String>();

            String incomingLine = Console.ReadLine();

            while(incomingLine != null)
            {
                fileContents.Add(incomingLine);

                incomingLine = Console.ReadLine();
            }

            
            #endregion

            #region Initial data output
        
            Console.WriteLine("Исходные данные:");
            
            foreach (var line in fileContents)
            {
                Console.WriteLine(line);
            }

            Console.WriteLine();

            #endregion

            #region Numeric Data Parsing and Formatting

            Decimal[] lineNumericContents = null;
            List<String> formattedResults = new List<String>();

            foreach (var line in fileContents)
            {
                lineNumericContents = line.ExtractNumbersAsDecimals();

                if (lineNumericContents != null)
                {
                    // Метка значения.
                    AlphabeticSymbol label = AlphabeticSymbol.A;

                    StringBuilder stringBuilder = new StringBuilder();

                    foreach (var number in lineNumericContents)
                    {
                        stringBuilder.Append(String.Format("{0}:{1} ", label, number));

                        // В том случае, если предыдущая метка была 'Z', осуществляется возврат к метке 'A'.
                        // В противном случае, осуществляется переход к следующему символу алфавита.
                        label = label == AlphabeticSymbol.Z ? AlphabeticSymbol.A : ++label;
                    }

                    formattedResults.Add(stringBuilder.ToString());
                }
            }

            #endregion

            #region Results analysis and output

            if (formattedResults.Count == 0)
            {
                Console.WriteLine("Валидные числа в файле не обнаружены!");
            }
            else
            {
                Console.WriteLine("Отформатированные результаты:");

                foreach (var result in formattedResults)
                {
                    Console.WriteLine(result);
                }
            }

            Console.ReadLine();
        }
        #endregion
    }
}
