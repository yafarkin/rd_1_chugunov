﻿using System;

namespace FakeMailer.Core
{
    /// <summary>
    /// Класс, реализующий сущность "Контакт".
    /// </summary>
    public class MailToData
    {
        #region Fields

        /// <summary>
        /// Фамилия контакта.
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Имя контакта.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Почтовый индекс контакта.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Область, в которой проживает контакт.
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Город, в котором проживает контакт.
        /// </summary>
        public string City { get; set; }

        #endregion

        #region Constructor

        public MailToData(string lastName, string firstName, string address)
        {
            Lastname = lastName;
            Firstname = firstName;

            string[] requisites = address.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (requisites?.Length == 3)
            {
                Zip = requisites[0].Trim();
                Region = requisites[1].Trim();
                City = requisites[2].Trim();
            }
            else
            {
                throw new ArgumentException("Неверно указан адрес", "Адрес вводится в формате: Индекс,область,город.");
            }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Переопределенная версия базового метода ToString(), возвращающая данные контакта.
        /// </summary>
        public override string ToString()
        {
            return $"Имя: {Lastname} {Firstname};{Environment.NewLine}Адрес: {Zip}, {Region}, {City}";
        }

        #endregion
    }
}



