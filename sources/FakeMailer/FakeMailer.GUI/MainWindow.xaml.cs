﻿using System;
using System.Windows;
using System.Windows.Media;

using FakeMailer.Core;

namespace FakeMailer.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    
        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            lblPrompt.Foreground = Brushes.Black;
            lblPrompt.Content = "Заполните поля и нажмите кнопку 'Показать'";
            lblContactInfo.Content = string.Empty;

            var lastName = txtLastname.Text.Trim();
            var firstName = txtFirstname.Text.Trim();
            var address = txtAddress.Text.Trim();

            if (lastName == string.Empty || firstName == string.Empty || address == string.Empty)
            {
                lblPrompt.Foreground = Brushes.Red;
                lblPrompt.Content = "Все поля должны быть заполнены.";
            }
            else
            {
                try
                {
                    MailToData contact = new MailToData(lastName, firstName, address);

                    lblContactInfo.Content = contact.ToString();
                }
                catch (ArgumentException ex)
                {
                    lblPrompt.Foreground = Brushes.Red;
                    lblPrompt.Content = ex.ParamName;
                }
            }
        }

        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите завершить работу с приложением?", "Подтвреждение выхода", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            lblPrompt.Foreground = Brushes.Black;
            lblPrompt.Content = "Заполните поля и нажмите кнопку 'Показать'";
            lblContactInfo.Content = string.Empty;

            txtLastname.Text = string.Empty;
            txtFirstname.Text = string.Empty;
            txtAddress.Text = string.Empty;

            txtFirstname.Focus();
        }
    }
}
