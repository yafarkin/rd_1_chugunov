﻿using System;
using FakeMailer.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FakeMailer.Tests
{
    [TestClass]
    public class MailToDataTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности обработки входных данных
        /// и возвращаемого строкового представления объекта.
        /// </summary>
        [TestMethod]
        public void MailToDataReturnsInfoCorrect()
        {
            var lastName = "Chugunov";
            var firstName = "Nikolay";
            var address = "445047, Samara Region, Tolyatti";

            var expectedResult = $"Имя: Chugunov Nikolay;{ Environment.NewLine }Адрес: 445047, Samara Region, Tolyatti";

            MailToData mailer = new MailToData(lastName, firstName, address);

            StringAssert.Equals(expectedResult, mailer.ToString());
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку выброса иключения ArgumentException
        /// в случае некорректного ввода адреса.
        /// </summary>
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void MailToDataThrowsAnExceptionIfDataIsIncorrect()
        {
            var lastName = "Chugunov";
            var firstName = "Nikolay";
            var address = "445047, Samara Region,";

            MailToData mailer = new MailToData(lastName, firstName, address);
        }
    }
}
