USE [Northwind]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================
-- Author:		Nikolay Chugunov
-- Create date: 24.01.2017
-- Description:	Stored procedure that receives an unique list of countries from [dbo].[Orders] table.
-- ==================================================================================================
CREATE PROCEDURE GetCountriesList
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT(ord.ShipCountry) 
	FROM [dbo].[Orders] ord
	ORDER BY ord.ShipCountry ASC
END
GO