USE [Northwind]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:		Nikolay Chugunov
-- Create date: 24.01.2017
-- Description:	Stored procedure that receives an unique list of orders from [dbo].[Orders] table.
-- ==================================================================================================
CREATE PROCEDURE GetOrdersList
	@pageSize INT = 100,
	@pageNumber INT = 1,
	@cityName NVARCHAR(MAX) = NULL,
	@countryName NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ord.OrderID, ord.OrderDate, ord.ShipCountry, ord.ShipCity,
		(emp.LastName + ' ' + emp.FirstName) AS EmployeeName,
		cust.CompanyName AS CustomerCompanyName
	FROM [dbo].[Orders] ord
	INNER JOIN [dbo].[Employees] emp ON ord.EmployeeID = emp.EmployeeID
	INNER JOIN [dbo].[Customers] cust ON ord.CustomerID = cust.CustomerID
	WHERE ord.ShipCity = CASE WHEN @cityName IS NOT NULL THEN @cityName
							  ELSE ord.ShipCity	
						 END
	AND  ord.ShipCountry = CASE WHEN @countryName IS NOT NULL THEN @countryName
							    ELSE ord.ShipCountry
						   END	 
	ORDER BY ord.OrderID ASC OFFSET @pageSize * (@pageNumber - 1) ROWS FETCH NEXT @pageSize ROWS ONLY; 
	END
