﻿using System;

namespace SimplySalesReporter.DAL.Entity
{
	/// <summary>
	/// Класс, реализующий сущность "Заказ".
	/// Служит оберткой для результатов выполнения хранимой процедуры на сервере.
	/// </summary>
	public class Order
	{
		/// <summary>
		/// Код заказа.
		/// </summary>
		public int OrderId { get; set; }

		/// <summary>
		/// Имя сотрудника, оформившго заказ.
		/// </summary>
		public string EmployeeName { get; set; }

		/// <summary>
		/// Название компании, сделавшей заказ.
		/// </summary>
		public string CustomerName { get; set; }

		/// <summary>
		/// Дата оформления заказа.
		/// </summary>
		public DateTime OrderDate { get; set; }

		/// <summary>
		/// Страна доставки заказа.
		/// </summary>
		public string Country { get; set; }

		/// <summary>
		/// Город доставки заказа.
		/// </summary>
		public string City { get; set; }
	}
}
