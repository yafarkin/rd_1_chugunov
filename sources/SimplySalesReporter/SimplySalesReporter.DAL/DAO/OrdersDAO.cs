﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using SimplySalesReporter.DAL.Entity;

namespace SimplySalesReporter.DAL.DAO
{
	/// <summary>
	/// Реализует доступ и обработку данных заказов.
	/// </summary>
	public class OrdersDAO
	{
		#region Fields

		/// <summary>
		/// Строка соединения с базой данных.
		/// </summary>
		private string _connectionString;

		#endregion

		#region Constructors

		public OrdersDAO()
		{
			_connectionString = ConfigurationManager.ConnectionStrings["SimplySalesReporterDataWarehouse"].ConnectionString;
		}

		public OrdersDAO(string connectionString)
		{
			_connectionString = connectionString;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Метод, реализующий получение списка заказов и назначенных на них продавцов.
		/// </summary>
		public IEnumerable<Order> GetOrders(string cityName = null, string countryName = null, int pageNumber = 1, int pageSize = 100)
		{
			var orders = new List<Order>();

			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();

				using (SqlCommand command = new SqlCommand("GetOrdersList", connection))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter cityNameParam = new SqlParameter();
					cityNameParam.ParameterName = "@cityName";
					cityNameParam.SqlDbType = SqlDbType.NChar;
					cityNameParam.Value = (object)cityName ?? DBNull.Value;
					cityNameParam.Direction = ParameterDirection.Input;

					command.Parameters.Add(cityNameParam);

					SqlParameter countryNameParam = new SqlParameter();
					countryNameParam.ParameterName = "@countryName";
					countryNameParam.SqlDbType = SqlDbType.NChar;
					countryNameParam.Value = (object)countryName ?? DBNull.Value;
					countryNameParam.Direction = ParameterDirection.Input;

					command.Parameters.Add(countryNameParam);

					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						orders.Add(new Order
						{
							OrderId = (int)reader["OrderId"],
							EmployeeName = (string)reader["EmployeeName"],
							CustomerName = (string)reader["CustomerCompanyName"],
							OrderDate = (DateTime)reader["OrderDate"],
							Country = (string)reader["ShipCountry"],
							City = (string)reader["ShipCity"]
						});
					}

					reader.Close();
				}

				return orders;
			}
		}

		#endregion
	}
}
