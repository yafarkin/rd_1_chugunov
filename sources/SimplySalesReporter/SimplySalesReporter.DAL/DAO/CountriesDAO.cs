﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SimplySalesReporter.DAL.DAO
{
	public class CountriesDAO
	{
		/// <summary>
		/// Строка соединения с базой данных.
		/// </summary>
		private string _connectionString { get; set; }

		#region Constructors

		public CountriesDAO()
		{
			_connectionString = ConfigurationManager.ConnectionStrings["SimplySalesReporterDataWarehouse"].ConnectionString;
		}

		public CountriesDAO(string connectionString)
		{
			_connectionString = connectionString;
		}

		#endregion


		public IEnumerable<string> GetCountries()
		{
			var countries = new List<string>();

			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();

				using (SqlCommand command = new SqlCommand("GetCountriesList", connection))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						countries.Add((string)reader["ShipCountry"]);
					}

					reader.Close();
				}
			}

			return countries;
		}

	}
}
