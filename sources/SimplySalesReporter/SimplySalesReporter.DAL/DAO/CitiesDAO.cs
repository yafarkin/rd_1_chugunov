﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SimplySalesReporter.DAL.DAO
{
	/// <summary>
	/// Реализует доступ и обработку данных городов.
	/// </summary>
	public class CitiesDAO
	{
		#region Fields

		/// <summary>
		/// Строка соединения с базой данных.
		/// </summary>
		private string _connectionString { get; set; }

		#endregion

		#region Constructors

		public CitiesDAO()
		{
			_connectionString = ConfigurationManager.ConnectionStrings["SimplySalesReporterDataWarehouse"].ConnectionString;
		}

		public CitiesDAO(string connectionString)
		{
			_connectionString = connectionString;
		}

		#endregion

		#region Methods
		
		/// <summary>
		/// Метод, реализующий получение уникального списка городов.
		/// </summary>
		public IEnumerable<string> GetCities()
		{
			var cities = new List<string>();

			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();

				using (SqlCommand command = new SqlCommand("GetCitiesList", connection))
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						cities.Add((string) reader["ShipCity"]);
					}

					reader.Close();
				}
			}

			return cities;
		}

		#endregion

	}
}
