﻿using System;
using System.Web.Mvc;

using SimplySalesReporter.WebUI.Models;

using SimplySalesReporter.DAL.DAO;

namespace SimplySalesReporter.WebUI.Controllers
{
	public class HomeController : Controller
	{
		private OrdersDAO _ordersDAO;
		private CitiesDAO _citiesDAO;
		private CountriesDAO _countriesDAO;

		public HomeController()
		{
			_ordersDAO = new OrdersDAO();
			_citiesDAO = new CitiesDAO();
			_countriesDAO = new CountriesDAO();
		}

		[HttpGet]
		public ActionResult Index()
		{
			var model = new OrderListViewModel
			{
				Orders = _ordersDAO.GetOrders(),
				Cities = new SelectList(_citiesDAO.GetCities()),
				Countries = new SelectList(_countriesDAO.GetCountries())
			};

			return View(model);
		}

		[HttpPost]
		public ActionResult Index(OrderListViewModel model)
		{
			var filteredModel = new OrderListViewModel
			{
				Orders = _ordersDAO.GetOrders(model.SelectedCity, model.SelectedCountry),
				Cities = new SelectList(_citiesDAO.GetCities()),
				Countries = new SelectList(_countriesDAO.GetCountries())
			};

			return View(filteredModel);
		}
	}
}