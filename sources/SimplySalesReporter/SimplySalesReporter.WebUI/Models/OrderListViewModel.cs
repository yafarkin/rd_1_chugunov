﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.Mvc;

using SimplySalesReporter.DAL.Entity;

namespace SimplySalesReporter.WebUI.Models
{
	/// <summary>
	/// Описывает модель фильтруемую модель представления, 
	/// отображающего список заказов с назначеннымин на них сотрудниками.
	/// </summary>
	public class OrderListViewModel
	{
		/// <summary>
		/// Название города, выбранного в качестве фильтра.
		/// </summary>
		[DisplayName("City")]
		public string SelectedCity { get; set; }

		/// <summary>
		/// Название страны, выбранной в качестве фильтра.
		/// </summary>
		[DisplayName("Country")]
		public string SelectedCountry { get; set; }

		/// <summary>
		/// Перечень названий городов для фильтрации заказов.
		/// </summary>
		public SelectList Cities { get; set; }

		/// <summary>
		/// Перечень названий стран для фильтрации заказов.
		/// </summary>
		public SelectList Countries { get; set; }

		/// <summary>
		/// Перечень заказов.
		/// </summary>
		public IEnumerable<Order> Orders { get; set; }
	}
}