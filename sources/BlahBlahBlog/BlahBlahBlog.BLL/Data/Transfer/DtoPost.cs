﻿using System;
using System.Collections.Generic;

namespace BlahBlahBlog.BLL.Data.Transfer
{
    /// <summary>
    /// Класс, реализующий объект для передачи данных между моделью уровня представления и уровнем доступа к данным.
    /// Трансформируется в сущность базы данных "Статья".
    /// </summary>
    public class DtoPost
    {
        /// <summary>
        /// Код статьи.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        /// Заголовок статьи.
        /// </summary>
        public string PostTitle { get; set; }

        /// <summary>
        /// Текст статьи.
        /// </summary>
        public string PostContent { get; set; }

        /// <summary>
        /// Анонс статьи (краткое содержание).
        /// </summary>
        public string PostExcerpt { get; set; }

        /// <summary>
        /// Дата публикации статьи.
        /// </summary>
        public DateTime PostDate { get; set; }

        /// <summary>
        /// Комментарии, закрепленные за статьёй.
        /// </summary>
        public List<DtoComment> Comments { get; set; }
    }
}
