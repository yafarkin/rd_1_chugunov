﻿using System;

namespace BlahBlahBlog.BLL.Data.Transfer
{
    /// <summary>
    /// Класс, реализующий объект для передачи данных между моделью уровня представления и уровнем доступа к данным.
    /// Трансформируется в сущность базы данных "Комментарий".
    /// </summary>
    public class DtoComment
    {
        /// <summary>
        /// Код комментария.
        /// </summary>
        public int CommentId { get; set; }

        /// <summary>
        /// Код статьи.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        /// Дата комментария.
        /// </summary>
        public DateTime CommentDate { get; set; }

        /// <summary>
        /// Имя автора комментария.
        /// </summary>
        public string CommentAuthorName { get; set; }

        /// <summary>
        /// Текст комментария.
        /// </summary>
        public string CommentText { get; set; }

        //public DtoPost Post { get; set; }
    }
}
