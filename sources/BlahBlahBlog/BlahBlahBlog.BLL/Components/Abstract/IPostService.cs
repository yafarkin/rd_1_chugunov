﻿using System;
using System.Collections.Generic;

using BlahBlahBlog.DAL.Data.Model;

using BlahBlahBlog.BLL.Data.Transfer;

namespace BlahBlahBlog.BLL.Components.Abstract
{
    /// <summary>
    /// Интерфейс, имлементируемый сервисом для работы со статьями.
    /// </summary>
    public interface IPostService : IService<Post>
    {
        /// <summary>
        /// Метод, реализующий публикацию новой статьи.
        /// </summary>
        void Publish(DtoPost post);

        /// <summary>
        /// Метод, реализующий получение всех имеющихся статей.
        /// </summary>
        IEnumerable<DtoPost> All();

        void Remove(int postID);

        DtoPost GetById(int id);

        void Update(DtoPost post);

        int Count();
    }
}
