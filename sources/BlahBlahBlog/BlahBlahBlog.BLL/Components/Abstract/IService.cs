﻿using System;

namespace BlahBlahBlog.BLL.Components.Abstract
{
    /// <summary>
    /// Наследуемый интерфейс, имплементируемый каждым специфическим сервисом.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности, с которым работает сервис.</typeparam>
    public interface IService<TEntity> where TEntity : class { }
}
