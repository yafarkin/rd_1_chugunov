﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlahBlahBlog.DAL.Data.Model;
using BlahBlahBlog.BLL.Data.Transfer;

namespace BlahBlahBlog.BLL.Components.Abstract
{
    public interface ICommentService : IService<Comment>
    {
        void Add(DtoComment comment);

        IEnumerable<DtoComment> All();
    }
}
