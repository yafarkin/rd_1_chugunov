﻿using System.Collections.Generic;

using AutoMapper;

using BlahBlahBlog.DAL.Data.Model;
using BlahBlahBlog.BLL.Data.Transfer;
using BlahBlahBlog.DAL.Components.Abstract;
using BlahBlahBlog.BLL.Components.Abstract;


namespace BlahBlahBlog.BLL.Components
{
    /// <summary>
    /// Класс, реализующий сервис для работы с комментариями.
    /// </summary>
    public class CommentService : ICommentService
    {
        #region Fields

        /// <summary>
        /// Экземпляр репозитория комментариев. 
        /// </summary>
        private ICommentRepository _commentRepository;

        #endregion

        #region Constructor

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        #endregion

        #region Methods

        public void Add(DtoComment comment)
        {
            _commentRepository.Add(Mapper.Map<Comment>(comment));
            _commentRepository.Commit();
        }

        public IEnumerable<DtoComment> All()
        {
            return Mapper.Map<List<DtoComment>>(_commentRepository.All());
        }

        #endregion
    }
}
