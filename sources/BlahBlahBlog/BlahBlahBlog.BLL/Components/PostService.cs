﻿using System;
using System.Linq;
using System.Collections.Generic;

using AutoMapper;

using BlahBlahBlog.BLL.Components.Abstract;
using BlahBlahBlog.BLL.Data.Transfer;

using BlahBlahBlog.DAL.Components.Abstract;
using BlahBlahBlog.DAL.Data.Model;


namespace BlahBlahBlog.BLL.Components
{
    /// <summary>
    /// Класс, реализующий сервис для работы со статьями.
    /// </summary>
    public class PostService : IPostService
    {
        #region Fields
        
        /// <summary>
        /// Экземпляр репозитория статей. 
        /// </summary>
        private IPostRepository _postRepository;

        #endregion

        #region Constructor

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Метод, реализующий публикацию новой статьи.
        /// </summary>
        public void Publish(DtoPost post)
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<DtoPost, Post>());

            var newPost = Mapper.Map<DtoPost, Post>(post);

            _postRepository.Add(newPost);
            _postRepository.Commit();
        }

        /// <summary>
        /// Метод, реализующий получение всех имеющихся статей.
        /// </summary>
        public IEnumerable<DtoPost> All()
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Post, DtoPost>());
            var posts = _postRepository.All();

            //Mapper.Initialize(cfg => cfg.CreateMap<Post, DtoPost>());

            return Mapper.Map<List<DtoPost>>(posts);
        }

        public int Count()
        {
            return _postRepository.Count();
        }

        public void Remove(int postID)
        {
            _postRepository.Remove(_postRepository.GetById(postID));
            _postRepository.Commit();
        }

        /// <summary>
        /// Метод, реализующий получение статьи по значению её идентификатора.
        /// </summary>
        public DtoPost GetById(int id)
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Post, DtoPost>());
            return Mapper.Map<DtoPost>(_postRepository.GetById(id));
        }

        public void Update(DtoPost post)
        {
            var editedPost = Mapper.Map<Post>(post);

            _postRepository.Update(editedPost);
            _postRepository.Commit();
        }
        #endregion
    }
}
