﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

using Microsoft.Practices.Unity;

using BlahBlahBlog.BLL.Components;
using BlahBlahBlog.BLL.Components.Abstract;

using BlahBlahBlog.DAL.Components;
using BlahBlahBlog.DAL.Components.Abstract;

using BlahBlahBlog.DAL.Data.Abstract;
using BlahBlahBlog.DAL.Data.Model;

namespace BlahBlahBlog.UI.Infrastructure
{
    public class UnityControllerFactory : DefaultControllerFactory
    {
        #region Fields

        /// <summary>
        /// Экземпляр контейнера зависимостей Unity.
        /// </summary>
        private UnityContainer _container;

        #endregion

        #region Constructor

        public UnityControllerFactory()
        {
            _container = new UnityContainer();

            RegisterTypes();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Переопределенная версия метода GetControllerInstance стандартной фабрики контроллеров,
        /// позволяющий получать экземпляр контроллера заданного типа.
        /// </summary>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
                   ? null
                   : _container.Resolve(controllerType) as IController;
        }

        /// <summary>
        /// Метод, реализующий задающий связь интерфейсов с их конкретной реализацией.
        /// Для каждого IInterface, Unity будет создавать сопоставленный ему Type.
        /// </summary>
        public void RegisterTypes()
        {
            _container.RegisterType<IPostService, PostService>();
            _container.RegisterType<IPostRepository, PostRepository>();
            _container.RegisterType<IDataWarehouseContext, EFDataWarehouse>();
            _container.RegisterType<ICommentService, CommentService>();
            _container.RegisterType<ICommentRepository, CommentRepository>();
        }

        #endregion
    }
}