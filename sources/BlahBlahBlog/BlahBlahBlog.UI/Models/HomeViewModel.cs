﻿using System;
using System.Collections.Generic;

using BlahBlahBlog.BLL.Data.Transfer;

namespace BlahBlahBlog.UI.Models
{
    /// <summary>
    /// Класс, реализующий модель представления "Домашняя страница".
    /// </summary>
    public class HomeViewModel
    {
        /// <summary>
        /// Статьи, имеющиеся в блоге.
        /// </summary>
        public IEnumerable<DtoPost> Posts { get; set; }

        /// <summary>
        /// Дополнительная информация для пейджинга.
        /// </summary>
        public PagingInfo PagingInfo { get; set; }
    }
}