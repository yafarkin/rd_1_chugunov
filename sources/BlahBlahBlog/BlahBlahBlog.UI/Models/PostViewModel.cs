﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BlahBlahBlog.UI.Models
{
    /// <summary>
    /// Модель представления работы со статьей.
    /// </summary>
    public class PostViewModel
    {
        /// <summary>
        /// Код статьи.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        /// Заголовок статьи.
        /// </summary>
        [Display(Name = "Заголовок статьи")]
        [Required(ErrorMessage = "Заголовок статьи должен быть указан.")]
        [MaxLength(100, ErrorMessage = "Длина заголовка статьи не должна превышать 100 символов!")]
        public string PostTitle { get; set; }

        /// <summary>
        /// Текст статьи.
        /// </summary>
        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        [Display(Name = "Текст статьи")]
        [Required(ErrorMessage = "Текст статьи должен быть указан.")]
        public string PostContent { get; set; }

        /// <summary>
        /// Краткое содержание статьи.
        /// </summary>
        [Display(Name = "Краткое содержание")]
        [Required(ErrorMessage = "Крактое содержание статьи должно быть указано.")]
        [MaxLength(255, ErrorMessage = "Длина заголовка статьи не должна превышать 255 символов!")]
        public string PostExcerpt { get; set; }

        /// <summary>
        /// Дата публикации статьи.
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime PostDate { get; set; }

        public List<CommentViewModel> Comments { get; set; }
    }
}