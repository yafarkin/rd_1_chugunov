﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlahBlahBlog.UI.Models
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }

        public int PostId { get; set; }

        public DateTime CommentDate { get; set; }

        [Required]
        [Display(Name = "Ваше имя")]
        public string CommentAuthorName { get; set; }

        [Required]
        [Display(Name = "Текст комментария")]
        public string CommentText { get; set; }
    }
}