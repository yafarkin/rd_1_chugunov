﻿using System;

namespace BlahBlahBlog.UI.Models
{
    /// <summary>
    /// Вспомогательный класс модели для пейджинга элементов.
    /// </summary>
    public class PagingInfo
    {
        /// <summary>
        /// Общее число элементов.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Количество элементов на странице.
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Номер текущей страницы.
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Общее количество страниц.
        /// </summary>
        public int TotalPages
        {
            get
            {
                return (int) Math.Ceiling((decimal)TotalItems / ItemsPerPage);
            }
        }
    }
}