﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlahBlahBlog.UI.Startup))]
namespace BlahBlahBlog.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
