﻿using System.Web.Mvc;

using AutoMapper;

using BlahBlahBlog.UI.Models;

using BlahBlahBlog.BLL.Components.Abstract;
using BlahBlahBlog.BLL.Data.Transfer;

namespace BlahBlahBlog.UI.Controllers
{
    public class CommentController : Controller
    {
        #region Fields

        /// <summary>
        /// Экземляр сервиса комментариев.
        /// </summary>
        private ICommentService _commentService;

        #endregion


        #region Constructor

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
       
        #endregion
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Create(int postId)
        {
            var model = new CommentViewModel { PostId = postId };

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CommentViewModel model)
        {
            var comment = Mapper.Map<DtoComment>(model);

            _commentService.Add(comment);

            return RedirectToAction("Read", "Post", new { id = comment.PostId });
        }
    }
}