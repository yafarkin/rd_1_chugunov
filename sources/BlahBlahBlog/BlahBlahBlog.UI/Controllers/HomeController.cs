﻿using System;
using System.Linq;
using System.Web.Mvc;

using BlahBlahBlog.UI.Models;

using BlahBlahBlog.BLL.Components.Abstract;

namespace BlahBlahBlog.UI.Controllers
{
    public class HomeController : Controller
    {
        #region Fields

        /// <summary>
        /// Экземпляр сервиса статей.
        /// </summary>
        private IPostService _postService;

        /// <summary>
        /// Количество постов на странице.
        /// </summary>
        public int PageSize { get; set; } = 10;

        #endregion

        #region Constructor

        public HomeController(IPostService postService)
        {
            _postService = postService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get-версия метода, реализующая отображение перечня статей с их пейджингом.
        /// </summary>
        /// <param name="pageNumber">Номер текущей страницы.</param>
        [HttpGet]
        public ActionResult Index(int pageNumber = 1)
        {
            var model = new HomeViewModel
            {
                Posts = _postService.All()
                                    .OrderByDescending(p => p.PostDate)
                                    .Skip((pageNumber - 1) * PageSize)
                                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = pageNumber,
                    ItemsPerPage = PageSize,
                    TotalItems = _postService.All().Count()
                }
            };

            return View(model);
        }

        #endregion
    }
}