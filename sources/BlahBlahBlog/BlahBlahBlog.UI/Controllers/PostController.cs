﻿using System;
using System.Web.Mvc;

using AutoMapper;

using BlahBlahBlog.UI.Models;

using BlahBlahBlog.BLL.Components.Abstract;
using BlahBlahBlog.BLL.Data.Transfer;


namespace BlahBlahBlog.UI.Controllers
{
    public class PostController : Controller
    {
        #region Fields

        /// <summary>
        /// Экземпляр сервиса статей.
        /// </summary>
        private IPostService _postService;

        #endregion

        #region Constructor

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get-версия метода, реализующая вывод требуемой статьи.
        /// </summary>
        [HttpGet]
        public ActionResult Read(int id)
        {
            var model = Mapper.Map<PostViewModel>(_postService.GetById(id));

            return View(model);
        }

        /// <summary>
        /// Get-версия метода, реализующая вызов представления для создания новой статьи.
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post-версия метода, реализующая публикацию новой статьи.
        /// </summary>
        [HttpPost]
        public ActionResult Create(PostViewModel model)
        {
            if (ModelState.IsValid)
            {
                var post = Mapper.Map<DtoPost>(model);

                _postService.Publish(post);

                return RedirectToAction("Index", "Home", null);
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// Get-версия метода, реализующая удаление статьи.
        /// </summary>
        [HttpGet]
        public ActionResult Remove(int id)
        {
            _postService.Remove(id);

            return RedirectToAction("Index", "Home", null);
        }

        /// <summary>
        /// Get-версия метода, реализующая вызов представления для редактирования статьи.
        /// </summary>
        [HttpGet]
        public ViewResult Edit(int id)
        {   
            var model = Mapper.Map<PostViewModel>(_postService.GetById(id));

            return View(model);
        }

        /// <summary>
        /// Post-версия метода, реализующая сохранение отредактированной статьи.
        /// </summary>
        [HttpPost]
        public ActionResult Edit(PostViewModel model)
        {
            if (ModelState.IsValid)
            {
                _postService.Update(Mapper.Map<DtoPost>(model));

                return RedirectToAction("Index", "Home", null);
            }
            else
            {
                return View();
            }
        }
        
        #endregion
    }
}