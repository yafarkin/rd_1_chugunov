﻿using System;

using AutoMapper;

using BlahBlahBlog.DAL.Data.Model;
using BlahBlahBlog.BLL.Data.Transfer;
using BlahBlahBlog.UI.Models;

namespace BlahBlahBlog.UI
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Post, DtoPost>().ReverseMap();
                cfg.CreateMap<Comment, DtoComment>().ReverseMap();
                cfg.CreateMap<DtoPost, PostViewModel>().ReverseMap();
                cfg.CreateMap<DtoComment, CommentViewModel>().ReverseMap();
            }); 
        }
    }
}