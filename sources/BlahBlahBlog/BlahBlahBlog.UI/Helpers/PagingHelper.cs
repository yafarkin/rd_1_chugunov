﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using BlahBlahBlog.UI.Models;

namespace BlahBlahBlog.UI.Helpers
{
    /// <summary>
    /// Вспомогательный класс, реализующий генерацию ссылок для пейджинга списка элементов.
    /// </summary>
    public static class PagingHelper
    {
        /// <summary>
        /// Метод, реализующий генерацию ссылки на страницу списка элементов.
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString GeneratePageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> url)
        {
            StringBuilder result = new StringBuilder();

            for(int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");

                tag.MergeAttribute("href", url(i));
                tag.InnerHtml = i.ToString();

                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                }

                result.Append(tag.ToString());
            }

            return MvcHtmlString.Create(result.ToString());
        }
    }
}