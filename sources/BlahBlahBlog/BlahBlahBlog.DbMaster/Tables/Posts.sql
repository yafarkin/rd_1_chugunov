﻿CREATE TABLE [dbo].[Posts]
(
	[PostId]		INT				NOT NULL	IDENTITY(1, 1), 
    [PostTitle]		NVARCHAR(100)	NOT NULL, 
    [PostContent]	NTEXT			NOT NULL, 
    [PostExcerpt]	NVARCHAR(255)	NOT NULL, 
    [PostDate]		DATETIME		NULL	DEFAULT getdate()

	CONSTRAINT PK_Posts_PostId PRIMARY KEY (PostId) 
)
