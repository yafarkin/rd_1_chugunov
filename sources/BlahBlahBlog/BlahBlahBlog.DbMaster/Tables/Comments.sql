﻿CREATE TABLE [dbo].[Comments]
(
	[CommentId]				INT		NOT NULL	IDENTITY(1, 1), 
    [PostId]				INT		NOT NULL, 
	[CommentAuthorName]		NVARCHAR(30)	NOT NULL, 
    [CommentText]			NTEXT	NOT NULL, 
    [CommentDate]			DATETIME NOT NULL

	CONSTRAINT PK_Comments_CommentId PRIMARY KEY (CommentId) DEFAULT getdate(),
	CONSTRAINT FK_Comments_PostId FOREIGN KEY (PostId)
		REFERENCES [dbo].[Posts](PostId) ON DELETE CASCADE, 
)
