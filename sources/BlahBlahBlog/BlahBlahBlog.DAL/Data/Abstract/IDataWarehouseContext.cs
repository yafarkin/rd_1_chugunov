﻿using System;
using System.Data.Entity;

namespace BlahBlahBlog.DAL.Data.Abstract
{
    /// <summary>
    /// Интерфейс, имплементируемый каждым из хранилищ данных.
    /// </summary>
    public interface IDataWarehouseContext : IDisposable
    {
        /// <summary>
        /// Метод получения набора сущностей определенного типа.
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности.</typeparam>
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Метод, реализующий сохранение изменений в хранилище данных.
        /// </summary>
        void SaveChanges();
    }
}
