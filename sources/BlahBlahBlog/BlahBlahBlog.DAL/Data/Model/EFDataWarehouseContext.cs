﻿using System;
using System.Data.Entity;

using BlahBlahBlog.DAL.Data.Abstract;

namespace BlahBlahBlog.DAL.Data.Model
{
    /// <summary>
    /// Класс-посредник между базой данных и уровнем DAL.
    /// </summary>
    public partial class EFDataWarehouse : DbContext, IDataWarehouseContext
    {
        /// <summary>
        /// Метод, реализующий получение типизированного набора сущностей базы данных.
        /// </summary>
        /// <typeparam name="TEntity">Тип хранимых сущностей.</typeparam>
        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        /// <summary>
        /// Метод, реализующий сохранение всех изменений в базу данных.
        /// </summary>
        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
