﻿using System;
using System.Linq;
using System.Collections.Generic;

using BlahBlahBlog.DAL.Data.Model;
using BlahBlahBlog.DAL.Data.Abstract;
using BlahBlahBlog.DAL.Components.Abstract;


namespace BlahBlahBlog.DAL.Components
{
    public class CommentRepository : ICommentRepository
    {
        private IDataWarehouseContext _dataWarehouse;

        public CommentRepository(IDataWarehouseContext dataWarehouse)
        {
            _dataWarehouse = dataWarehouse;
        }

        public void Add(Comment item)
        {
            _dataWarehouse.Set<Comment>().Add(item);
        }

        public IEnumerable<Comment> All()
        {
            return _dataWarehouse.Set<Comment>().AsEnumerable();
        }

        public Comment Get(Comment item)
        {
            return _dataWarehouse.Set<Comment>().Find(item);
        }

        public Comment GetById(int id)
        {
            return _dataWarehouse.Set<Comment>()
                .Where(c => c.CommentId == id)
                .FirstOrDefault();
        }

        public void Remove(Comment item)
        {
            _dataWarehouse.Set<Comment>().Remove(item);
        }

        public void Update(Comment item)
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            _dataWarehouse.SaveChanges();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }
    }
}
