﻿using System;
using System.Collections.Generic;
using System.Linq;

using BlahBlahBlog.DAL.Data.Abstract;
using BlahBlahBlog.DAL.Data.Model;
using BlahBlahBlog.DAL.Components.Abstract;

namespace BlahBlahBlog.DAL.Components
{
    /// <summary>
    /// Класс, реализующий репозиторий для работы со статьями.
    /// </summary>
    public class PostRepository : IPostRepository
    {
        #region Fields

        /// <summary>
        /// Экземпляр хранилища данных.
        /// </summary>
        private IDataWarehouseContext _dataWarehouse;

        #endregion

        #region Constructor

        public PostRepository(IDataWarehouseContext dataWarehouse)
        {
            _dataWarehouse = dataWarehouse;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий получение сущности по значению ее идентификатора.
        /// </summary>
        /// <param name="id">Идентификатор требуемой сущности.</param>
        public Post GetById(int id)
        {
            return _dataWarehouse.Set<Post>()
                .Find(id);
        }

        /// <summary>
        /// Метод, реализующий получение заданной сущности.
        /// ??????????
        /// </summary>
        /// <param name="item">Требуемая сущность.</param>
        public Post Get(Post item)
        {
            return _dataWarehouse.Set<Post>()
               .Find(item.PostId);
        }

        public int Count()
        {
            return _dataWarehouse.Set<Post>().Count();
        }

        /// <summary>
        /// Метод, реализующий получение полного набора сущностей.
        /// </summary>
        public IEnumerable<Post> All()
        {
            return _dataWarehouse.Set<Post>().AsEnumerable();
        }

        /// <summary>
        /// Метод, реализующий добавление новой сущности.
        /// </summary>
        public void Add(Post item)
        {
            _dataWarehouse.Set<Post>().Add(item);
        }

        /// <summary>
        /// Метод, реализующий обновление заданной сущности.
        /// </summary>
        public void Update(Post item)
        {
            var entry = GetById(item.PostId);

            entry.PostTitle = item.PostTitle;
            entry.PostContent = item.PostContent;
            entry.PostExcerpt = item.PostExcerpt;
            entry.PostDate = item.PostDate;
        }

        /// <summary>
        /// Метод, реализующий удаление заданной сущности.
        /// </summary>
        public void Remove(Post item)
        {
            _dataWarehouse.Set<Post>().Remove(item);
        }

        /// <summary>
        /// Метод, реализующий сохранение изменений в источник данных.
        /// </summary>
        public void Commit()
        {
            _dataWarehouse.SaveChanges();
        }

        #endregion
    }
}
