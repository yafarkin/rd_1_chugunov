﻿using System;

using BlahBlahBlog.DAL.Data.Model;

namespace BlahBlahBlog.DAL.Components.Abstract
{
    /// <summary>
    /// Интерфейс, имлементируемый репозиторием статей.
    /// </summary>
    public interface IPostRepository : IRepository<Post> { }
}
