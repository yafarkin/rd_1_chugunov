﻿using System;

using System.Collections.Generic;

namespace BlahBlahBlog.DAL.Components.Abstract
{
    /// <summary>
    /// Наследуемый интерфейс, имплементируемый каждым специфическим репозиторием.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности, с которым работает репозиторий.</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Метод, реализующий получение экземпляра сущности по значению ее идентификатора.
        /// </summary>
        /// <param name="id">Идентификатор требуемой сущности.</param>
        TEntity GetById(int id);

        /// <summary>
        /// Метод, реализующий получение заданной сущности.
        /// </summary>
        /// <param name="item">Требуемая сущность.</param>
        TEntity Get(TEntity item);

        /// <summary>
        /// Метод, реализующий получение полного набора сущностей.
        /// </summary>
        IEnumerable<TEntity> All();

        /// <summary>
        /// Метод, реализующий добавление новой сущности в набор.
        /// </summary>
        void Add(TEntity item);

        /// <summary>
        /// Метод, реализующий обновление заданной сущности.
        /// </summary>
        void Update(TEntity item);

        /// <summary>
        /// Метод, реализующий удаление заданной сущности из набора.
        /// </summary>
        void Remove(TEntity item);

        /// <summary>
        /// Метод, реализующий сохранение изменений в источнике данных.
        /// </summary>
        void Commit();

        int Count();
    }
}
