﻿using System;

using BlahBlahBlog.DAL.Data.Model;

namespace BlahBlahBlog.DAL.Components.Abstract
{
    /// <summary>
    /// Интерфейс, имплементируемый репозиторием комментариев.
    /// </summary>
    public interface ICommentRepository : IRepository<Comment> { }
}
