﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;

using Moq;

using BlahBlahBlog.BLL.Components.Abstract;
using BlahBlahBlog.BLL.Data.Transfer;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

using BlahBlahBlog.UI.Controllers;
using BlahBlahBlog.UI.Models;
using BlahBlahBlog.UI.Helpers;

namespace BlahBlahBlog.UnitTests
{
    /// <summary>
    /// Тестовый класс, реализующий методы, тестирующие функциональность уровня пользователя.
    /// </summary>
    [TestClass]
    public class PresentationLayerTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности пагинации статей на странице.
        /// </summary>
        [TestMethod]
        public void HomeControllerPaginatesCorrect()
        {
            Mock<IPostService> postService = new Mock<IPostService>();

            postService.Setup(p => p.All()).Returns(new List<DtoPost>
            {
                new DtoPost {
                    PostId = 1,
                    PostTitle = "Hello, World!",
                    PostContent = "Lorem Ipsum Dolor Si Amlet",
                    PostExcerpt = "Lorem Ipsum",
                    PostDate = DateTime.Now
                },
                new DtoPost
                {
                    PostId = 2,
                    PostTitle = "Hello, World!",
                    PostContent = "Lorem Ipsum Dolor Si Amlet",
                    PostExcerpt = "Lorem Ipsum",
                    PostDate = DateTime.Now
                },
                new DtoPost
                {
                    PostId = 3,
                    PostTitle = "What is Repo?",
                    PostExcerpt = "Repo is pattern",
                    PostContent = "Repo is pattern which can give you more flexibility",
                    PostDate = DateTime.Now,
                },
            });

            HomeController controller = new HomeController(postService.Object);

            controller.PageSize = 2;

            HomeViewModel result = (HomeViewModel)controller.Index(2).Model;

            DtoPost[] posts = result.Posts.ToArray();

            Assert.IsTrue(posts.Length == 1);
            Assert.AreEqual(posts[0].PostId, 3);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректности генерации ссылок при пейджинге элементов.
        /// </summary>
        [TestMethod]
        public void PageHelperGeneratesLinksCorrect()
        {
            HtmlHelper myHelper = null;

            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            Func<int, string> pageUrlDelegate = n => $"Page{ n }";

            var result = myHelper.GeneratePageLinks(pagingInfo, pageUrlDelegate);

            var expectedResult =  @"<a href=""Page1"">1</a>"
                                + @"<a class=""selected"" href=""Page2"">2</a>"
                                + @"<a href=""Page3"">3</a>";

            var actualResult = result.ToString();

            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректности информации о нумерации страниц,
        /// передаваемой в представление.
        /// </summary>
        [TestMethod]
        public void PostControllerSendsPaginationInfoCorrect()
        {
            Mock<IPostService> postService = new Mock<IPostService>();

            postService.Setup(p => p.All()).Returns(new List<DtoPost>
            {
                new DtoPost {
                    PostId = 1,
                    PostTitle = "Hello, World!",
                    PostContent = "Lorem Ipsum Dolor Si Amlet",
                    PostExcerpt = "Lorem Ipsum",
                    PostDate = DateTime.Now
                },
                new DtoPost
                {
                    PostId = 2,
                    PostTitle = "Hello, World!",
                    PostContent = "Lorem Ipsum Dolor Si Amlet",
                    PostExcerpt = "Lorem Ipsum",
                    PostDate = DateTime.Now
                },
                new DtoPost
                {
                    PostId = 3,
                    PostTitle = "What is Repo?",
                    PostExcerpt = "Repo is pattern",
                    PostContent = "Repo is pattern which can give you more flexibility",
                    PostDate = DateTime.Now,
                },
                new DtoPost
                {
                    PostId = 4,
                    PostTitle = "What is Repository?",
                    PostExcerpt = "Repo is structure pattern",
                    PostContent = "Repo is pattern which can give you more flexibility in development",
                    PostDate = DateTime.Now,
                },
            });

            HomeController target = new HomeController(postService.Object);
            target.PageSize = 3;

            HomeViewModel result = (HomeViewModel)target.Index(2).Model;

            PagingInfo pageInfo = result.PagingInfo;

            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 4);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }


    }
}
