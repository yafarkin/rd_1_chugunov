﻿using System;
using System.Collections.Generic;
using System.Linq;

using Moq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Data.Entity;

using BlahBlahBlog.BLL.Data.Transfer;
using BlahBlahBlog.BLL.Components;
using BlahBlahBlog.BLL.Components.Abstract;
using BlahBlahBlog.DAL.Components.Abstract;
using BlahBlahBlog.DAL.Data.Abstract;
using BlahBlahBlog.DAL.Data.Model;

namespace BlahBlahBlog.UnitTests
{
    [TestClass]
    public class BusinessLogicLayerTests
    {
        [TestMethod]
        public void PostServicePublishesNewPostCorrect()
        {
            Mock<IPostRepository> dataWarehouse = new Mock<IPostRepository>();

            Mock<IDbSet<Post>> mockSet = new Mock<IDbSet<Post>>();

            var posts = new List<Post>
            {
                new Post
                {
                    PostTitle = "What is Repo?",
                    PostExcerpt = "Repo is pattern",
                    PostContent = "Repo is pattern which can give you more flexibility",
                    PostDate = DateTime.Now,
                 }
            }.AsQueryable();

            mockSet.As<IQueryable<Post>>().Setup(m => m.Provider).Returns(posts.Provider);
            mockSet.As<IQueryable<Post>>().Setup(m => m.Expression).Returns(posts.Expression);
            mockSet.As<IQueryable<Post>>().Setup(m => m.ElementType).Returns(posts.ElementType);
            mockSet.As<IQueryable<Post>>().Setup(m => m.GetEnumerator()).Returns(posts.GetEnumerator());

            Mock<IDataWarehouseContext> mockContext = new Mock<IDataWarehouseContext>();

            mockContext.Setup(c => c.Set<Post>()).Returns(mockSet.Object);

            Mock<IPostRepository> repo = new Mock<IPostRepository>();

            repo.Setup(r => r.Add(new Post()));

            var postService = new PostService(repo.Object);

            var post = new DtoPost
            {
                PostTitle = "IAMSTUPID",
                PostContent = "LALALA",
                PostDate = DateTime.Now,
                PostExcerpt = "Excerpt right here",
            };

            postService.Publish(post);
        }
    }
}
