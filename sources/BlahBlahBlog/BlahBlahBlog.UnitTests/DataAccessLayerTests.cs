﻿using System;
using System.Linq;
using System.Data.Entity;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using BlahBlahBlog.DAL.Components;
using BlahBlahBlog.DAL.Data.Model;

namespace BlahBlahBlog.UnitTests
{
    [TestClass]
    public class DataAccessLayerTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректного добавления нового поста в контекст хранилища данных.
        /// </summary>
        [TestMethod]
        public void EFDataWarehouseAddsNewPostToDatabaseCorrect()
        { 
            // Arrange.
            EFDataWarehouse dbContext = new EFDataWarehouse();

            Post post = new Post
            {
                PostTitle = "Hello, World!",
                PostContent = "Lorem Ipsum Dolor Si Amlet",
                PostExcerpt = "Lorem Ipsum",
            };

            // Act.
            dbContext.Set<Post>().Add(post);

            var expectedResult = post.PostId;

            var actualResult = dbContext.Set<Post>()
                .Where(p => p.PostId == expectedResult)
                .Select(p => p.PostId)
                .FirstOrDefault();

            // Assert.
            Assert.AreEqual(expectedResult, actualResult);

        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректного удаления поста из контекста хранилища данных.
        /// </summary>
        [TestMethod]
        public void EFDataWarehouseRemovesPostCorrect()
        {
            // Arrange.
            EFDataWarehouse dataWarehouse = new EFDataWarehouse();

            Post newPost = new Post
            {
                PostTitle = "Hello, World!",
                PostContent = "Lorem Ipsum Dolor Si Amlet",
                PostExcerpt = "Lorem Ipsum",
                PostDate = DateTime.Now
            };

            // Act.
            dataWarehouse.Set<Post>().Add(newPost);

            dataWarehouse.Set<Post>().Remove(newPost);
            dataWarehouse.SaveChanges();

            // Assert.
            var actualResult = dataWarehouse.Set<Post>()
                .Where(p => p.PostId == newPost.PostId)
                .FirstOrDefault();

            Assert.IsNull(actualResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректного обновления записи в контексте хранилища данных.
        /// </summary>
        [TestMethod]
        public void EFDataWarehouseUpdatesPostCorrect()
        {
            // Arrange.
            EFDataWarehouse dataWarehouse = new EFDataWarehouse();

            Post newPost = new Post
            {
                PostTitle = "Hello, World!",
                PostContent = "Lorem Ipsum Dolor Si Amlet",
                PostExcerpt = "Lorem Ipsum",
                PostDate = DateTime.Now
            };

            var expectedResult = "Dolor Si Amlet";

            // Act.
            dataWarehouse.Set<Post>().Add(newPost);
            dataWarehouse.SaveChanges();

            newPost.PostTitle = "Dolor Si Amlet";

            dataWarehouse.Entry(newPost).State = EntityState.Modified;
            dataWarehouse.SaveChanges();

            var actualResult = dataWarehouse.Set<Post>()
                .Where(p => p.PostId == newPost.PostId)
                .Select(p => p.PostTitle)
                .FirstOrDefault();

            // Assert.
            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректного добавления данных репозиторием постов.
        /// </summary>
        [TestMethod]
        public void PostRepositoryAddsNewPostCorrect()
        {
            // Arrange.
            var postRepository = new PostRepository(new EFDataWarehouse());

            var newPost = new Post
            {
                PostTitle = "What is Repos?",
                PostExcerpt = "Repo is pattern",
                PostContent = "Repo is pattern which can give you more flexibility",
            };

            // Act.
            postRepository.Add(newPost);
            postRepository.Commit();

            var expectedResult = newPost.PostId;
            var actualResult = postRepository.Get(newPost).PostId;
            
            // Assert.
            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку корректного удаления записи репозиторием постов.
        /// </summary>
        [TestMethod]
        public void PostRepostioryRemovesPostCorrect()
        {
            // Arrange.
            var postRepository = new PostRepository(new EFDataWarehouse());

            var newPost = new Post
            {
                PostTitle = "What is Repo?",
                PostExcerpt = "Repo is pattern",
                PostContent = "Repo is pattern which can give you more flexibility",
                PostDate = DateTime.Now,
            };

            // Act.
            postRepository.Add(newPost);
            postRepository.Commit();

            postRepository.Remove(newPost);
            postRepository.Commit();

            var actualResult = postRepository.Get(newPost);

            // Assert.
            Assert.IsNull(actualResult);
        }

        [TestMethod]
        public void PostRepositoryUpdatesPostCorrect()
        {
            // Arrange.
            var postRepository = new PostRepository(new EFDataWarehouse());

            var newPost = new Post
            {
                PostTitle = "What is Repo?",
                PostExcerpt = "Repo is pattern",
                PostContent = "Repo is pattern which can give you more flexibility",
                PostDate = DateTime.Now,
            };

            // Act.
            postRepository.Add(newPost);
            postRepository.Commit();

            newPost.PostTitle = "What is Repository?";

            postRepository.Update(newPost);
            postRepository.Commit();

            var actualResult = postRepository.Get(newPost).PostTitle;

            // Assert.
            Assert.AreEqual(actualResult, "What is Repository?");
        }
    }
}
