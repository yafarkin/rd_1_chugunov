﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SampleSupport;
using Task;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private readonly DataSource dataSource = new DataSource();

        private bool ValidateZipCode(string val)
        {
            long number;
            return long.TryParse(val, out number);
        }

        private bool ValidatePhone(string phone)
        {
            var pattern = @"^(\([0-9]\))+?";
            return Regex.IsMatch(phone, pattern);
        }

        private List<GroupPriceEntity> SortProductsByPrice()
        {
            var sortedProducts = new List<GroupPriceEntity>();

            foreach (var prod in dataSource.Products)
                if (prod.UnitPrice <= 20M)
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 0 });
                else if ((prod.UnitPrice > 20M) && (prod.UnitPrice <= 50M))
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 1 });
                else
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 2 });

            return sortedProducts;
        }


        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Описание задачи на русском языке")
        ]
        public void LinqForExample()
        {
            var customers = dataSource
                .Customers
                .Select(c => c);

            //Он умеет выполнять запросы самостоятельно. Так же умеет делать foreach по коллекции. 
            //Проверьте это все запустив приложение и сделав запуск запроса
            ObjectDumper.Write(customers);
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #1")]
        [Description("Выдача списка клиентов с суммарным оборотом, превосходящим величину X.")]
        public void Linq001()
        {
            var criterrias = new decimal[] { 10000.00m, 14200.50m, 500.55m, 100000.00m };

            foreach (var criterria in criterrias)
            {
                ObjectDumper.Write($"Перечень клиентов с суммарным оборотом, превосходящим { criterria }:");

                var query = dataSource
                    .Customers
                    .Where(c => c.Orders.Sum(o => o.Total) > criterria)
                    .Select(r => new
                    {
                        CustomerId = r.CustomerID,
                        TotalTurnover = r.Orders.Sum(o => o.Total)
                    })
                    .OrderBy(r => r.TotalTurnover);

                ObjectDumper.Write(query);
                ObjectDumper.Write("-------------------------------------------------------");

            }
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #2")]
        [Description("Выдача списка поставщиков, находящихся в одной стране и городе со своими клиентами.")]
        public void Linq002()
        {
            var query = dataSource
                .Customers
                .Join
                (
                    dataSource.Suppliers,
                    Customer => Customer.City,
                    Supplier => Supplier.City,
                    (customer, supplier) => new
                    {
                        CustomerName = customer.CompanyName,
                        CustomerLocation = customer.City,
                        SupplierName = supplier.SupplierName,
                        SupplierLocation = supplier.City
                    }
                )
                .OrderBy(c => c.CustomerLocation);

            ObjectDumper.Write("Перечень поставщиков, находящихся в одной стране и городе со своими клиентами:");
            ObjectDumper.Write(query);
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #3")]
        [Description("Выдача списка клиетов, имеющих заказы, превосходящие по сумме величину X:")]
        public void Linq003()
        {
            var criterrias = new decimal[] { 5000.00m, 250.00m, 4000.52m };

            foreach (var criterria in criterrias)
            {
                var query = dataSource
                    .Customers
                    .SelectMany(c => c.Orders, (customer, order) => new
                    {
                        OrderID = order.OrderID,
                        CustomerName = customer.CompanyName,
                        OrderDate = order.OrderDate,
                        OrderTotal = order.Total
                    })
                    .Where(o => o.OrderTotal > criterria)
                    .OrderBy(o => o.OrderTotal);

                    ObjectDumper.Write($"Перечень клиентов с заказами со стоимостью, превосходящей { criterria }:");

                ObjectDumper.Write(query);

                ObjectDumper.Write("-------------------------------------------------------");
            }
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #4")]
        [Description("Выдача списка клиентов с указанием даты начала отношений.")]
        public void Linq004()
        {
            var query = dataSource
                .Customers
                .SelectMany(c => c.Orders, (customer, order) => new
                {
                    CustomerName = customer.CompanyName,
                    // Дата первой сделки. Принимается за дату начала финансовых отношений.
                    RelationshipSince = order.OrderDate
                })
                .GroupBy(f => f.CustomerName)
                    .Select(g => g.OrderBy(d => d.RelationshipSince)
                    .First());

            ObjectDumper.Write("Перечень клиентов с указанием даты начала отношений");
            ObjectDumper.Write(query);
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #5")]
        [Description("Выдача списка клиентов с указанием даты начала отношений, отсортированный по году, месяцу и общему обороту")]
        public void Linq005()
        {
            var query = dataSource
                .Customers
                .SelectMany(c => c.Orders, (customer, order) => new
                {
                    CustomerName = customer.CompanyName,
                    // Дата первой сделки. Принимается за дату начала финансовых отношений.
                    RelationshipSince = order.OrderDate,
                    Turnover = customer.Orders.Sum(o => o.Total)
                })
                .GroupBy(f => f.CustomerName)
                    .Select(g => g.OrderBy(d => d.RelationshipSince).First())
                .OrderBy(d => d.RelationshipSince.Year)
                .ThenBy(d => d.RelationshipSince.Month)
                .ThenByDescending(d => d.Turnover);

            ObjectDumper.Write("Перечень клиентов с указанием даты начала отношений, отсортированный по году, месяцу и общему обороту:");
            ObjectDumper.Write(query);
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #6")]
        [Description("Выдача списка клиентов с отсутствующим регионом или кодом оператора в телефоне, или нецифровым почтовым индексом.")]
        public void Linq006()
        {
            var query = dataSource
                .Customers
                .Select(c => new
                {
                    CustomerName = c.CompanyName,
                    Phone = c.Phone,
                    Region = c.Region,
                    PostalCode = c.PostalCode
                })
                .Where(c => c.Region == null
                         || !c.Phone.StartsWith("(")
                         || !c.PostalCode.Contains(@"^1-9"));

            ObjectDumper.Write("Перечень клиентов с отсутствующим регионом или кодом оператора в телефоне, или нецифровым почтовым индексом.");
            ObjectDumper.Write(query);
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #8")]
        [Description("Выдача списка товаров, сгруппированных в ценовые категории.")]
        public void Linq008()
        {
            var query = SortProductsByPrice()
                .GroupBy(p => p.Group);

            foreach (var products in query)
            {
                var priceCategory = products.Key == 0 ? "Дешевая" : products.Key == 1 ? "Умеренная" : "Дорогая";

                ObjectDumper.Write($"Ценовая группа: { priceCategory }");

                foreach (var product in products)
                {
                    ObjectDumper.Write(product.product);
                }
            }
        }

        [Category("Домашнее задание по Linq")]
        [Title("Задание #9")]
        [Description("Расчёт средней прибыльности и интенсивности заказов в каждом городе.")]
        public void Linq009()
        {
            var query = dataSource
                .Customers
                .GroupBy(c => c.City)
                    .Select(c => new
                    {
                        CityName = c.Key,
                        Stats = c
                                .Select(d => new
                                {
                                    ClientName = d.CompanyName,
                                    TotalOrdersSum = d.Orders.Sum(o => o.Total),
                                    TotalOrdersCount = d.Orders.Count()
                                })
                    })
                    .Select(v => new
                    {
                        CityName = v.CityName,
                        OrdersSumAverage = Math.Round(v.Stats.Sum(s => s.TotalOrdersSum) / v.Stats.Sum(s => s.TotalOrdersCount), 2),
                        CustomerActivityAverage = v.Stats.Sum(s => s.TotalOrdersCount) / v.Stats.Count()
                    });

            ObjectDumper.Write("Средняя прибыльность и интенсивность заказов в городах:");
            ObjectDumper.Write(query);
        }

    }
}