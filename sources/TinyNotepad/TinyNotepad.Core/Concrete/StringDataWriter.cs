﻿using System;
using System.IO;

using TinyNotepad.Core.Abstract;

namespace TinyNotepad.Core.Concrete
{
    /// <summary>
    /// Класс, реализующий запись текстовых данных в файл.
    /// </summary>
    public class StringDataWriter : IOutput<string>
    {
        #region Fields

        /// <summary>
        /// Путь к файлу.
        /// </summary>
        private string _filePath;

        #endregion

        #region Constructor

        public StringDataWriter(string filePath)
        {
            try
            {
                using (var writer = new StreamWriter(filePath)) { }
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException("Путь к файлу не был задан.", ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException("Путь к файлу содержит недопустимые символы или пуст.", ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new DirectoryNotFoundException("Неверно указан путь или часть пути.", ex);
            }
            catch (PathTooLongException ex)
            {
                throw new PathTooLongException("Недопустимая длина пути к файлу.", ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new UnauthorizedAccessException("Отсутствие доступа к файлу/папке.", ex);
            }

            _filePath = filePath;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий непосредственную запись текстовых данных в файл.
        /// </summary>
        /// <param name="data">Данные для записи.</param>
        public void Write(string data)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(_filePath, false))
                {
                    writer.Write(data);
                }
            }
            catch (IOException ex)
            {
                throw new IOException("Ошибка ввода/вывода.", ex);
            }
        }
    }
    #endregion
}
