﻿using System;
using System.IO;

using TinyNotepad.Core.Abstract;

namespace TinyNotepad.Core.Concrete
{
    /// <summary>
    /// Класс, реализующий функционал чтения текстовых данных из файла.
    /// </summary>
    public class StringDataReader : IInput<string>
    {
        #region Fields

        /// <summary>
        /// Путь к файлу.
        /// </summary>
        private string _filePath;

        #endregion

        #region Constructor

        public StringDataReader(string filePath)
        {
            try
            {
                using (var reader = new StreamReader(filePath)) { }
            }
            
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException("Путь к файлу не был задан.", ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException("Путь содержит недопустимые знаки или пуст.", ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw new DirectoryNotFoundException("Неверно указан путь или часть пути.", ex);
            }
            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException("Запрашиваемого файла не существует.", ex);
            }
            catch (PathTooLongException ex)
            {
                throw new PathTooLongException("Недопустимая длина пути к файлу.", ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new UnauthorizedAccessException("Доступ к файлу отсутствует.", ex);
            }

            _filePath = filePath;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий непосредственное считывание данных из файла.
        /// </summary>
        public string Read()
        {
            try
            {
                using (var reader = new StreamReader(_filePath))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (IOException ex)
            {
                throw new IOException("Ошибка ввода/вывода.", ex);
            }
        }

        #endregion
    }
}
