﻿using System;
using System.IO;

namespace TinyNotepad.Core.Abstract
{
    public interface IOutput<T>
    {
        /// <summary>
        /// Метод, реализующий непосредственную запись данных.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="DirectoryNotFoundException"></exception>
        /// <exception cref="PathTooLongException"></exception>
        /// <exception cref="IOException"></exception>
        void Write(T data);
    }
}
