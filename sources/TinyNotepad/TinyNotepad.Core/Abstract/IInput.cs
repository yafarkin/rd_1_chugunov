﻿using System;
using System.IO;

namespace TinyNotepad.Core.Abstract
{
    public interface IInput<T>
    {
        /// <summary>
        /// Метод, реализующий непосредственное чтение данных.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception> 
        /// <exception cref="UnauthorizedAccessException"></exception>
        /// <exception cref="DirectoryNotFoundException"></exception>
        /// <exception cref="FileNotFoundException"></exception> 
        /// <exception cref="PathTooLongException"></exception>
        /// <exception cref="IOException"></exception>
        T Read();
    }
}
