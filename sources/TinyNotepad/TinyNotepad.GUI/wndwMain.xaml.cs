﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Documents;

using Microsoft.Win32;

using TinyNotepad.Core.Abstract;
using TinyNotepad.Core.Concrete;

namespace TinyNotepad.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        /// <summary>
        /// Название приложения. Используется в заголовках диалоговых окон и главного окна приложения.
        /// </summary>
        private readonly string _appName = System.Windows.Forms.Application.ProductName;

        /// <summary>
        /// Адрес текущего файла.
        /// Используется для того, чтобы определять, был ли сохранен создаваемый файл хотя бы однажды.
        /// </summary>
        private string _currentFilePath = null;

        /// <summary>
        /// Имя текущего файла.
        /// Используется в заголовке окна.
        /// </summary>
        private string _currentFileName = "Новый документ";

        /// <summary>
        /// Признак наличия несохраненных данных.
        /// </summary>
        private bool _gotUnsavedData = false;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();

            this.Title = $"{ _currentFileName } - { _appName }";

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Обработчик события выбора пункта меню "Файл->Создать".
        /// </summary>
        private void menuSubItemFileCreate_Click(object sender, RoutedEventArgs e)
        {
            if (_gotUnsavedData)
            {
                switch (SaveConfirmation())
                {
                    case MessageBoxResult.Yes:
                        menuSubItemFileSave_Click(this, new RoutedEventArgs());
                        CreateNewFileInstance();
                        break;

                    case MessageBoxResult.No:
                        CreateNewFileInstance();
                        break;

                    default:
                        rchtxtData.Focus();
                        break;
                }
            }
            else
            {
                CreateNewFileInstance();
            }
        }

        /// <summary>
        /// Обработчик события выбора пункта меню "Файл->Открыть".
        /// </summary>
        private void menuSubItemFileOpen_Click(object sender, RoutedEventArgs e)
        {
            if (_gotUnsavedData)
            {
                var saveConfirmationResult = SaveConfirmation();

                switch (saveConfirmationResult)
                {
                    case MessageBoxResult.Yes:
                        menuSubItemFileSave_Click(this, new RoutedEventArgs());
                        OpenFileInstance();
                        break;

                    case MessageBoxResult.No:
                        OpenFileInstance();
                        break;

                    default:
                        rchtxtData.Focus();
                        break;
                }
            }
            else
            {
                OpenFileInstance();
            }
        }

        /// <summary>
        /// Обработчик события выбора пункта меню "Файл->Сохранить".
        /// </summary>
        private void menuSubItemFileSave_Click(object sender, RoutedEventArgs e)
        {
            if (_gotUnsavedData && _currentFilePath != null)
            {
                var fileInfo = new FileInfo(_currentFilePath);

                if (fileInfo.IsReadOnly)
                {
                    menuSubItemFileSaveAs_Click(this, new RoutedEventArgs());
                }
                else
                {
                    try
                    {
                        IOutput<string> _fileDataWriter = new StringDataWriter(_currentFilePath);

                        var fileContents = new TextRange(rchtxtData.Document.ContentStart, rchtxtData.Document.ContentEnd);

                        _fileDataWriter.Write(fileContents.Text);

                        _gotUnsavedData = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, _appName, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                if (_currentFilePath == null)
                {
                    menuSubItemFileSaveAs_Click(this, new RoutedEventArgs());
                }
            }
        }

        /// <summary>
        /// Обработчик события выбора пункта меню "Файл->Сохранить как".
        /// </summary>
        private void menuSubItemFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                FileName = _currentFileName,
                DefaultExt = "*.txt",
                CheckPathExists = true,
                ValidateNames = true,
                OverwritePrompt = true,
                Filter = "Текстовые файлы|*.txt" + "|Все файлы|*.*",
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                IOutput<string> _fileDataWriter = new StringDataWriter(saveFileDialog.FileName);

                try
                {
                    var fileContents = new TextRange(rchtxtData.Document.ContentStart, rchtxtData.Document.ContentEnd);

                    _fileDataWriter.Write(fileContents.Text);

                    _currentFilePath = saveFileDialog.FileName;
                    _currentFileName = Path.GetFileName(saveFileDialog.FileName);

                    _gotUnsavedData = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, _appName, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    wndwMain.Title = $"{ _currentFileName } - { _appName }";
                    rchtxtData.Focus();
                }
            }
        }

        /// <summary>
        /// Обработчик события выбора пункта меню "Файл->Выход".
        /// </summary>
        private void menuSubItemQuit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void rchtxtData_KeyDown(object sender, RoutedEventArgs e)
        {
            _gotUnsavedData = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_gotUnsavedData)
            {
                var dialogResult = MessageBox.Show($"Вы хотите сохранить изменения в файле: { _currentFileName }?", _appName,
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                switch(dialogResult)
                {
                    case MessageBoxResult.Yes:
                        if (_currentFilePath == null)
                        {
                            menuSubItemFileSaveAs_Click(this, new RoutedEventArgs());
                        }
                        else
                        {
                            menuSubItemFileSave_Click(this, new RoutedEventArgs());
                        }
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        rchtxtData.Focus();
                        break;
                }
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Метод, реализующий создание нового файла.
        /// </summary>
        private void CreateNewFileInstance()
        {
            _gotUnsavedData = false;

            _currentFileName = "Новый документ";
            _currentFilePath = null;

            wndwMain.Title = $"{ _currentFileName } - { _appName }";

            rchtxtData.Document.Blocks.Clear();
            rchtxtData.Focus();
        }

        /// <summary>
        /// Метод, реализующий открытие загрузку файла.
        /// </summary>
        private void OpenFileInstance()
        {
            var openFileDialog = new OpenFileDialog
            {
                CheckFileExists = false,
                CheckPathExists = false,
                Multiselect = false,
                Filter = "Текстовые файлы|*.txt" + "|Все файлы|*.*"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    IInput<string> _fileDataReader = new StringDataReader(openFileDialog.FileName);

                    var fileContents = new TextRange(rchtxtData.Document.ContentStart, rchtxtData.Document.ContentEnd);

                    fileContents.Text = _fileDataReader.Read();

                    _currentFileName = Path.GetFileName(openFileDialog.FileName);
                    _currentFilePath = openFileDialog.FileName;

                    _gotUnsavedData = false;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    wndwMain.Title = $"{ _currentFileName } - { _appName }";
                    rchtxtData.Focus();
                }
            }
        }

        private MessageBoxResult SaveConfirmation()
        {
            return MessageBox.Show($"Вы хотите сохранить изменения в файле { _currentFileName }?", _appName,
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
        }

        #endregion 
    }
}
