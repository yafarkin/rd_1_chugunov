﻿using System;
using System.IO;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TinyNotepad.Core.Abstract;
using TinyNotepad.Core.Concrete;

namespace TinyNotepad.Tests
{
    [TestClass]
    public class StringDataWriterTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности записи текстовых данных в файл.
        /// </summary
        [TestMethod]
        public void StringDataWriterWritesDataCorrect()
        {
            var data = "This is test string";
            var filePath = @"dataWriterTest.txt";

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);

            stringDataWriter.Write(data);

            var actualResult = string.Empty;

            using (StreamReader reader = new StreamReader(filePath))
            {
                actualResult = reader.ReadToEnd();
            }

            File.Delete(filePath);

            Assert.AreEqual(data, actualResult);

        }

        /// <summary>
        /// Тестовый метод, осуществляющий осуществляющий контроль выброса исключения ArgumentNullException
        /// в случае, если путь к файлу равен null.
        /// </summary>
        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void StringDataWriterThrowsAnExceptionIfPathIsNull()
        {
            string filePath = null;

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения ArgumentException
        /// в случае, если путь к файлу - пустая строка.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StringDataWriterThrowsAnExceptionIfPathIsEmpty()
        {
            var filePath = string.Empty;
            var textToWrite = "some text to be written";

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StringDataWriterThrowsAnExceptionIfPathContainsInvalidChars()
        {
            var filePath = "****|just/file|*****";
            var textToWrite = "some text to be written";

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения DirectoryNotFoundException
        /// в случае некорректно указанного пути к файлу, или его части.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void StringDataWriterThrowsAnExceptionIfPathIsInvalid()
        {
            var filePath = @"cocor00z@\123.txt";
            var textToWrite = "some text to be written";

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения UnauthorizedAccessException
        /// в случае попытки получения данных из файла с закрытым доступом.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(UnauthorizedAccessException))]
        public void StringDataWriterThrowsAnExceptionIfUnauthorizedAccess()
        {
            string filePath = $"{ Path.GetPathRoot(Environment.SystemDirectory)}/System Volume Information";
            string textToWrite = "some text to be written";

            IOutput<string> stringDataWriter = new StringDataWriter(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения PathTooLongException
        /// в том случае, если путь для записи файла имеет большую длину, чем предусмотрено в системе.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PathTooLongException))]
        public void StringDataWriterThrowsAnExceptionIfPathTooLong()
        {
            var filePath = @"C:\Jack\Sphinx\Loves\My\Qwartz\Too\Lorem\Ipsum\Dolor\Sit\Amet\Honor.txt";
            var textToWrite = "some text to be written";

            var badPathBuilder = new StringBuilder(filePath);

            for (int i = 0; i < 10; i++)
            {
                badPathBuilder.Append(filePath);
            }

            IOutput<string> stringDataWriter = new StringDataWriter(badPathBuilder.ToString());

            stringDataWriter.Write(textToWrite);
        }
    }
}