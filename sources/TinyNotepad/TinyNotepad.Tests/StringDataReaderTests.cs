﻿using System;
using System.IO;
using System.Security;
using System.Security.Permissions;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TinyNotepad.Core.Abstract;
using TinyNotepad.Core.Concrete;

namespace TinyNotepad.Tests
{
    [TestClass]
    public class StringDataReaderTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности чтения текстовых данных из файла.
        /// </summary>
        [TestMethod]
        public void StringDataReaderReadsDataCorrect()
        {
            var filePath = @"test.tst";
            var expectedResult = "This is my expected result for stringDataReader test.";

            using (var writer = new StreamWriter(filePath, false))
            {
                writer.Write(expectedResult);
            }

            IInput<string> stringDataReader = new StringDataReader(filePath);

            var actualResult = stringDataReader.Read();

            File.Delete(filePath);

            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий выброс исключения NullReferenceException
        /// в том случае, если путь к файлу равен null.
        /// </summary>
        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void StringDataReaderThrowsAnExceptionIfPathIsNull()
        {
            string filePath = null;

            IInput<string> stringDataReader = new StringDataReader(filePath);

        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения ArgumentException 
        /// в случае наличия недопустимых символов в пути к файлу.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StringDataReaderThrowsAnExceptionIfFilePathContainsInvalidChars()
        {
            var filePath = @"*\****|file.txt|*****";

            IInput<string> stringDataReader = new StringDataReader(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения ArgumentException
        /// для пустого пути к файлу.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StringDataReaderThrowsAnExceptionIfFilePathIsEmpty()
        {
            var filePath = string.Empty;

            IInput<string> dataReader = new StringDataReader(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения FileNotFoundException
        /// в случае отсутствия указанного текстового файла в файловой системе.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void StringDataReaderThrowsAnExceptionIfFileNotExists()
        {
            var filePath = @"152433.txt";

            IInput<string> stringDataReader = new StringDataReader(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения DirectoryNotFoundException
        /// в случае некорректно указанного пути к файлу, или его части.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void StringDataReaderThrowsAnExceptionIfPathIsInvalid()
        {
            var filePath = @"skillz00rza\123.txt";

            IInput<string> stringDataReader = new StringDataReader(filePath);

        }


        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения UnauthorizedAccessException
        /// в случае попытки получения данных из файла или директории с закрытым доступом.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(UnauthorizedAccessException))]
        public void StringDataReaderThrowsAnExceptionIfUnauthorizedAccess()
        {
            string filePath = $"{ Path.GetPathRoot(Environment.SystemDirectory) } /System Volume Information";
            
            IInput<string> stringDataReader = new StringDataReader(filePath);
        }

        /// <summary>
        /// Тестовый метод, осуществляющий контроль выброса исключения PathTooLongException
        /// в том случае, если путь для чтения файла имеет большую длину, чем предусмотрено в системе.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PathTooLongException))]
        public void StringDataReaderThrowsAnExceptionIfPathTooLong()
        {
            string filePath = @"\Jack\Sphinx\Loves\My\Qwartz\Too\Lorem\Ipsum\Dolor\Sit\Amet\Honor.txt";

            StringBuilder badPathBuilder = new StringBuilder(filePath);
            
            for (int i = 0; i < 10; i++)
            {
                badPathBuilder.Append(filePath);
            }

            IInput<string> stringDataReader = new StringDataReader(badPathBuilder.ToString());
        }

    }
}