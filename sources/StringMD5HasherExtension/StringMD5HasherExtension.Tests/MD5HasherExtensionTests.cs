﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using StringMD5HasherExtension.Core;

namespace StringMD5HasherExtension.Tests
{
    /// <summary>
    /// Тестовый класс, инкапсулирующий методы тестирования функциональности, 
    /// реализуемой классом MD5HasherExtension.
    /// </summary>
    [TestClass]
    public class MD5HasherExtensionTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку возврата одинакового md5-хеша
        /// для одной и той же строки при каждом выполнении операции его получения.
        /// </summary>
        [TestMethod]
        public void GetMD5HashReturnsSameResultForTheSameLine()
        {
            var line = "This is MD5HasherExtension Test line";

            var expectedResult = line.GetMD5Hash();
            var actualResult = line.GetMD5Hash();

            CollectionAssert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку возврата разлчных md5-хешей для двух разных строк.
        /// </summary>
        [TestMethod]
        public void GetMD5HashReturnsDifferentResultForDifferentLines()
        {
            var line = "This is MD5HasherExtension test line. ";

            var initialLineResult = line.GetMD5Hash();

            line += "Hello!";

            var editedLineResult = line.GetMD5Hash();

            CollectionAssert.AreNotEqual(initialLineResult, editedLineResult);
        }
    }
}
