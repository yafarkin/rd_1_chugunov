﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace StringMD5HasherExtension.Core
{
    /// <summary>
    /// Класс, реализующий метод(ы) расширения базового класса String.
    /// </summary>
    public static class MD5HasherExtension
    {
        /// <summary>ф
        /// Метод, реализующий вычисление md5-хеша экземпляра класса System.String.
        /// </summary>
        public static byte[] GetMD5Hash(this string line)
        {
            return new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(line));
        }
    }
}
