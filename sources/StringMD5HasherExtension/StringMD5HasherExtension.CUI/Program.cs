﻿using System;
using System.Text;

using StringMD5HasherExtension.Core;

namespace StringMD5HasherExtension.CUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var line = string.Empty;

            Console.Write("Введите строку для вычисления хэш-кода по алгоритму md5:");

            do
            {
                line = Console.ReadLine().Trim();
            }
            while (line == string.Empty);

            Console.WriteLine();

            var md5hash = line.GetMD5Hash();
            var stringMaster = new StringBuilder();

            // Формирование результитующей строки хеш-кода,
            // с 16-ричным представлением каждого его разряда.
            foreach (var unit in md5hash)
            {
                stringMaster.Append($"{ unit.ToString("x2") } ");
            }

            //---------------------------------------------------
          
            Console.WriteLine($"MD5-хэш строки: { stringMaster.ToString() }");

            Console.ReadLine();
        }
    }
}
