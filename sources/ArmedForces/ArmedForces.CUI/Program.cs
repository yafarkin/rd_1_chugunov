﻿using System;
using System.Threading;

using ArmedForces.Core;

namespace ArmedForces.CUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Thread.Sleep(2000);

            var SAS = new CombatUnit("Special Air Service", 500);

            MilitaryForces.CombatUnits.Add(SAS);

            Console.WriteLine($"{ SAS.Name } was created with strength: { SAS.UnitStrength }");
            Console.WriteLine($"Total armed forces strength after { SAS.Name } creation: { MilitaryForces.RegularForcesTotalStrength }");
            Console.WriteLine();

            var losses = 0;

            var hasVictored = SAS.MakeACombat(out losses);

            var combatResult = hasVictored ? "won" : "condenced";

            Console.WriteLine("Oh, no! Terrorists attacked citizens!!");

            Thread.Sleep(1000);

            Console.WriteLine($"{ SAS.Name } is taking a combat...");

            Thread.Sleep(1500);

            Console.WriteLine();

            Console.WriteLine($"Combat results: { SAS.Name } { combatResult }");
            Console.WriteLine($"{ SAS.Name } losses: { losses }");
            Console.WriteLine($"{ SAS.Name } unit strength after losses: { SAS.UnitStrength }");
            Console.WriteLine($"Total armed forces strength after {SAS.Name} losses: { MilitaryForces.RegularForcesTotalStrength }");

            Thread.Sleep(1500);

            Console.WriteLine();

            var GIGN = new CombatUnit("GIGN-9", 115);

            Console.WriteLine($"{ GIGN.Name } was created with strength: { GIGN.UnitStrength }");
            Console.WriteLine($"Total armed forces strength after { GIGN.Name } creation: { MilitaryForces.RegularForcesTotalStrength }");

            MilitaryForces.CombatUnits.Add(GIGN);

            var replenishmentNumber = 1200;

            Console.WriteLine();

            Console.WriteLine($"{ SAS.Name } got a replenishment: { replenishmentNumber }.");
            SAS.GiveReplenishment(replenishmentNumber);

            Thread.Sleep(1000);

            Console.WriteLine($"{ SAS.Name } unit strength after replenishment: { SAS.UnitStrength }");
            Console.WriteLine($"Total armed forces strength after { SAS.Name } replenishment: { MilitaryForces.RegularForcesTotalStrength }");

            Thread.Sleep(1500);

            Console.WriteLine();

            Console.WriteLine($"{ GIGN.Name } got a replenishment: { replenishmentNumber }.");
            GIGN.GiveReplenishment(replenishmentNumber);

            Thread.Sleep(1000);

            Console.WriteLine();

            Console.WriteLine($"GIGN-9 unit strength after replenishment: { GIGN.UnitStrength }");
            Console.WriteLine($"Total armed forces strength after GIGN-9 replenishment: { MilitaryForces.RegularForcesTotalStrength }");

            Thread.Sleep(1000);

            Console.WriteLine();

            Console.WriteLine($"There are { MilitaryForces.CombatUnits.Count } regular forces units in the city:");

            foreach (var unit in MilitaryForces.CombatUnits)
            {
                Console.WriteLine($"{ unit.Name }");
            }

            Console.WriteLine();
            Console.WriteLine("Secutrity is under control!");

            Console.ReadLine();
        }
    }
}
