﻿using System;

namespace ArmedForces.Core
{
    /// <summary>
    /// Класс методов расширений.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Метод, реализующий генерирование случайной булевой величины.
        /// </summary>
        public static bool NextBoolean(this Random randomer)
        {
            randomer = new Random();

            return Convert.ToBoolean(randomer.Next(0, 2));
        }
    }
}
