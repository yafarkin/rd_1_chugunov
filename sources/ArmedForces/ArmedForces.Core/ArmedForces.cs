﻿using System;
using System.Collections.Generic;

namespace ArmedForces.Core
{
    /// <summary>
    /// Класс, реализующий сущность "Вооруженные силы".
    /// </summary>
    public abstract class MilitaryForces
    {
        /// <summary>
        /// Численность регулярных войск.
        /// </summary>
        public static long RegularForcesTotalStrength { get; protected set; }

        /// <summary>
        /// Виды войск, входящие в регулярную армию.
        /// </summary>
        public static List<CombatUnit> CombatUnits { get; set; }

        static MilitaryForces()
        {
            CombatUnits = new List<CombatUnit>();
        }

        /// <summary>
        /// Ме
        /// </summary>
        /// <param name="losses"></param>
        /// <returns></returns>
        public abstract bool MakeACombat(out int losses);

    }

    /// <summary>
    /// Класс, реализующий сущность "Вид войск", 
    /// происходящую от сущности "Вооруженные силы".
    /// </summary>
    public class CombatUnit : MilitaryForces
    {
        /// <summary>
        /// Название вида войск.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Численность вида войск.
        /// </summary>
        public int UnitStrength { get; private set; }


        public CombatUnit(string name, int strength)
        {
            Name = name;
            UnitStrength = strength;
            RegularForcesTotalStrength += strength;
        }

        /// <summary>
        /// Метод, реализующий пополнение войск.
        /// </summary>
        /// <param name="strength"></param>
        public void GiveReplenishment(int strength)
        {
            UnitStrength += strength;
            RegularForcesTotalStrength += strength;
        }

        /// <summary>
        /// Метод, реализующий проведение сражения видом войск.
        /// </summary>
        /// <param name="losses">Число потерь.</param>
        public override bool MakeACombat(out int losses)
        {
            Random randomer = new Random();

            losses = randomer.Next(0, UnitStrength);

            UnitStrength -= losses;
            RegularForcesTotalStrength -= losses;

            return randomer.NextBoolean();
        }
    }
    
}
