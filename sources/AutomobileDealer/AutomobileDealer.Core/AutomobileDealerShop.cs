﻿using System;
using System.Collections.Generic;

namespace AutomobileDealer.Core
{
    /// <summary>
    /// Статический класс, реализующий сущность "Автосалон".
    /// </summary>
    public static class AutomobileDealerShop
    {
        #region Fields

        /// <summary>
        /// Автомобили, имеющиеся в салоне.
        /// </summary>
        public static List<Automobile> Stock { get; set; }

        #endregion
    }
}
