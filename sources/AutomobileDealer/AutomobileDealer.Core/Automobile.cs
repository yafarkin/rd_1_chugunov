﻿using System;

namespace AutomobileDealer.Core
{
    /// <summary>
    /// Класс, реализующий сущность "Автомобиль".
    /// </summary>
    public class Automobile
    {
        #region Fields

        /// <summary>
        /// Двигатель автомобиля.
        /// </summary>
        private AutomobileEngine _engine;

        /// <summary>
        /// Название компании-производителя автомобиля.
        /// </summary>
        public string Manufacturer { get; }

        /// <summary>
        /// Название модели автомобиля.
        /// </summary>
        public string Model { get; }

        /// <summary>
        /// Цвет автомобиля.
        /// </summary>
        public string Color { get; }

        #endregion

        #region Constructor

        public Automobile(decimal enginePower, string manufacturer, string model, string color)
        {
            _engine = new AutomobileEngine(enginePower);

            Manufacturer = manufacturer;
            Model = model;
            Color = color;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий запуск автомобиля.
        /// </summary>
        public bool Start()
        {
           return _engine.Start();
        }

        /// <summary>
        /// Переопределенная версия базового метода ToString(), 
        /// возвращающая информацию об автомобиле в виде: [Производитель] [Модель] [Цвет] [Мощность двигателя].
        /// </summary>
        public override string ToString()
        {
            return $"{ Manufacturer } { Model }, Color: { Color }, Engine power: { _engine.Power }";
        }

        #endregion
    }
}


