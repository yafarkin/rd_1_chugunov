﻿using System;

namespace AutomobileDealer.Core
{
    /// <summary>
    /// Класс, реализующий сущность "Двигатель автомобиля".
    /// </summary>
    public class AutomobileEngine
    {
        #region Properties

        /// <summary>
        /// Мощность двигателя.
        /// </summary>
        public decimal Power { get; }

        #endregion

        #region Constructor

        public AutomobileEngine(decimal power)
        {
            Power = power;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий запуск двигателя.
        /// </summary>
        public bool Start()
        {
            return new Random().NextBoolean();
        }
     
        #endregion


    }
}
