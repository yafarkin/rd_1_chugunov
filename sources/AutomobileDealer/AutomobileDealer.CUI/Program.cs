﻿using System;
using System.Collections.Generic;
using System.Threading;
using AutomobileDealer.Core;

namespace AutomobileDealer.CUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            AutomobileDealerShop.Stock = new List<Automobile>
                                             {
                                                new Automobile(enginePower:85m, manufacturer:"LADA", model:"Kalina 2", color:"Izabella"),
                                                new Automobile(enginePower:55m, manufacturer:"VAZ", model:"2106", color:"Papyrus"),
                                                new Automobile(enginePower:250m, manufacturer:"KIA", model:"Sportage 2016", color:"Dark night"),
                                                new Automobile(enginePower:500m, manufacturer:"Lamborghini", model:"Aventador", color:"Yellow Riverside")
                                             };

            Console.WriteLine("Welcome to RoyalMotors! Today, we've got something special for you!");
            Console.WriteLine();

            Thread.Sleep(1500);

            Console.WriteLine($"There are { AutomobileDealerShop.Stock.Count } cars:");

            foreach (var automobile in AutomobileDealerShop.Stock)
            {
                Console.WriteLine(automobile);
            }

            Console.WriteLine();
            Console.WriteLine("Let's test them!");
            Console.WriteLine();

            Thread.Sleep(1500);

            foreach (var automobile in AutomobileDealerShop.Stock)
            {
                var result = automobile.Start();

                Thread.Sleep(1500);

                Console.WriteLine(result == true 
                                  ? $"{ automobile.Manufacturer } {automobile.Model} sounds like a cat! Make a test-drive now!" 
                                  : $"Oops, something wrong with { automobile.Manufacturer } {automobile.Model}!");
                Console.WriteLine();
            }

            Thread.Sleep(1500);

            Console.WriteLine("Already leaving? Well, let us know when you make a decision! We'll be glad to see you soon. Bye!");

            Console.ReadLine();
        }
    }
}
