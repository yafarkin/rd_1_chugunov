﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using GenericCollection.Core;

namespace GenericCollection.UnitTests
{
    [TestClass]
    public class GenericCollectionTests
    {
        [TestMethod]
        public void GenericCollectionConstructorCreatesEmptyCollectionCorrect()
        {
            var expectedResult = 0;

            var collection = new GenericCollection<byte>();

            Assert.AreEqual(expectedResult, collection.Count);

        }

        [TestMethod]
        public void GenericCollectionConstructorCreatesNItemsCollectionCorrect()
        {
            var expectedResult = 5;

            var collection = new GenericCollection<byte>(5);

            Assert.AreEqual(expectedResult, collection.Count);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GenericCollectionThrowsAnExceptionIfCountIsNegative()
        {
            var collection = new GenericCollection<int>(-5);
        }

        [TestMethod]
        public void GenericCollectionAddItemsCorrect()
        {
            var expectedResult = 900;

            var collection = new GenericCollection<int>();

            collection.Add(expectedResult);

            Assert.AreEqual(expectedResult, collection[0]);
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void GenericCollectionAddThrowsAnExceptionIfCollectionIsReadOnly()
        {
            var collection = new GenericCollection<DateTime>();

            collection.IsReadOnly = true;

            collection.Add(new DateTime(2011, 10, 14, 10, 45, 52));
        }

        [TestMethod]
        public void GenericCollectionClearsCorrect()
        {
            var expectedResult = 3;
            var collection = new GenericCollection<int> { 1, 2, 3 };

            Assert.AreEqual(expectedResult, collection.Count);

            expectedResult = 0;

            collection.Clear();

            Assert.AreEqual(expectedResult, collection.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void GenericCollectionThrowsAnExceptionWhenClearIfCollectionIsReadOnly()
        {
            var collection = new GenericCollection<int> { 1, 2, 3 };

            collection.IsReadOnly = true;

            collection.Clear();
        }

        [TestMethod]
        public void GenericCollectionContainsCorrect()
        {
            var collection = new GenericCollection<int> { 100, 200, 300 };

            var actualResult = collection.Contains(60000000);

            Assert.IsFalse(actualResult);

            actualResult = collection.Contains(200);

            Assert.IsTrue(actualResult);

            int[] arr = { 1, 2, 3, 4, 5 };

            var oneMorecollection = new GenericCollection<int[]>
            {
                new int[] { 100, 200, 300 },
                arr
            };

            actualResult = oneMorecollection.Contains(arr);

            Assert.IsTrue(actualResult);

            actualResult = oneMorecollection.Contains(new int[] { 1, 2, 3, 4, 5 });

            Assert.IsFalse(actualResult);
        }

        [TestMethod]
        public void GenericCollectionCopyToExecutesCorrect()
        {
            var arr = new int[6];

            arr[0] = 100;
            arr[1] = 200;
            arr[2] = 300;
            arr[3] = 400;

            var collection = new GenericCollection<int> { 999, 555, 333, 555 };

            collection.CopyTo(arr, 2);

            var expectedResult = new int[] { 100, 200, 999, 555, 333, 555 };

            CollectionAssert.AreEqual(expectedResult, arr);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GenericCollectionCopyToThrowsAnExceptionIfArrayPointerIsNull()
        {
            int[] arr = null;

            var collection = new GenericCollection<int> { 100, 200 };

            collection.CopyTo(arr, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericCollectionCopyToThrowsAnExceptionIfArrayIndexIsLessThanZero()
        {
            int[] arr = new int[] { 1, 2, 3 };

            var collection = new GenericCollection<int> { 100, 200 };

            collection.CopyTo(arr, -2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GenericCollectionCopyToThrowsAnExceptionIfCollectionSizeIsGreaterThanArraySize()
        {
            int[] arr = new int[] { 1, 2, 3 };

            var collection = new GenericCollection<int> { 100, 200, 500, 800, 900 };

            collection.CopyTo(arr, 6);
        }

        [TestMethod]
        public void GenericCollectionRemoveExecutesCorrect()
        {
            var collection = new GenericCollection<int> { 5, 5, 2, 4, 3, 1 };
            var itemToRemove = 4;

            collection.Remove(itemToRemove);

            Assert.IsFalse(collection.Contains(itemToRemove));

        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void GenericCollectionRemoveThrowsAnExceptionIfCollectionIsReadOnly()
        {
            var collection = new GenericCollection<int> { 5, 5, 2, 4, 3, 1 };

            collection.IsReadOnly = true;

            collection.Remove(4);

        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericCollectionGetIndexatorThrowsAnExceptionIfIndexIsLessThanZero()
        {
            var collection = new GenericCollection<byte> { 1, 2, 3, 4, 122 };

            var a = collection[-1];
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericCollectionGetIndexatorThrowsAnExceptionIfIndexIsLessThanCollectionCount()
        {
            var collection = new GenericCollection<byte> { 1, 2, 3, 4, 122 };

            var a = collection[19000];
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericCollectionSetIndexatorThrowsAnExceptionIfIndexIsLessThanZero()
        {
            var collection = new GenericCollection<byte> { 1, 2, 3, 4, 122 };

            collection[-1] = 100;
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void GenericCollectionSetIndexatorThrowsAnExceptionIfIndexIsLessThanCollectionCount()
        {
            var collection = new GenericCollection<byte> { 1, 2, 3, 4, 122 };

            collection[90000] = 5;
        }
    }
}
