﻿using System;

using GenericCollection.Core;
namespace GenericCollection.CUI
{
    public class Program
    {
        static void Main(string[] args)
        {
            var collection = new GenericCollection<int> { 1, 2, 3, 4, 5 };

            collection.CollectionEvent += CollectionChange;

            DisplayCollectionState(collection);

            Console.WriteLine($"collection.IsReadOnly:{ collection.IsReadOnly }");
            Console.WriteLine($"collection.Count:{ collection.Count }");
            Console.WriteLine($"collection.Contains(3):{ collection.Contains(3) }");

            Console.WriteLine();

            var anyArray = new int[8];

            Console.Write("Any array was created:");

            anyArray[0] = 100;
            anyArray[1] = 200;
            anyArray[2] = 300;
            anyArray[3] = 400;
            
            foreach (var item in anyArray)
            {
                Console.Write($"{ item } ");
            }

            Console.WriteLine();
            Console.WriteLine();

            collection.CopyTo(anyArray, 3);

            Console.Write("collection.CopyTo(anyArray,3):");

            foreach (var item in anyArray)
            {
                Console.Write($"{ item } ");
            }

            Console.WriteLine();
            Console.WriteLine();

            collection.Add(1);

            DisplayCollectionState(collection);

            collection.Remove(4);

            DisplayCollectionState(collection);

            Console.WriteLine("var requiredItem = collection[3]");

            Console.WriteLine();

            var requiredItem = collection[3];

            Console.WriteLine("collection[4] = 999");

            Console.WriteLine();

            collection[4] = 999;

            DisplayCollectionState(collection);

            Console.ReadLine();
        }

        /// <summary>
        /// Метод, реализующий вывод содержимого коллекции.
        /// </summary>
        static void DisplayCollectionState<T>(GenericCollection<T> collection)
        {
            Console.Write("Collection state:");

            foreach (var item in collection)
            {
                Console.Write($"{ item } ");
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        /// <summary>
        /// Обработчик событий коллекции.
        /// </summary>
        static void CollectionChange(int item, EventType eventType)
        {
            Console.WriteLine($"item with value { item } was { eventType } ");
            Console.WriteLine();
        }
    }
}
