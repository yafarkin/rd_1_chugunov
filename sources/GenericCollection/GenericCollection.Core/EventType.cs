﻿using System;

namespace GenericCollection.Core
{
    /// <summary>
    /// Перечисление видов событий коллекции.
    /// </summary>
    public enum EventType : byte
    {
        /// <summary>
        /// Элемент был добавлен.
        /// </summary>
        Added = 1,

        /// <summary>
        /// Элемент коллекции был изменен.
        /// </summary>
        Updated = 2,

        /// <summary>
        /// Элемент коллекции был удалён.
        /// </summary>
        Removed = 3,

        /// <summary>
        /// Элемент коллекции был запрошен (получен).
        /// </summary>
        Received = 4
    }

}
