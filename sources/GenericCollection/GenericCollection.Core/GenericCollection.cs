﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GenericCollection.Core
{
    public delegate void ItemSetChanged<T>(T item, EventType eventType);

    /// <summary>
    /// Класс, реализующий коллекцию-шаблон.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericCollection<T> : IEnumerable<T>, ICollection<T>
    {
        private T[] _contents;

        public ItemSetChanged<T> CollectionEvent;

        #region Constructors

        /// <summary>
        /// Стандартный конструктор без параметров.
        /// </summary>
        public GenericCollection()
        {
            _contents = new T[0];
        }

        /// <summary>
        /// Конструктор, создающий новый объект-коллекцию заданного размера.
        /// </summary>
        /// <param name="count">Количество элементов в коллекции.</param>
        public GenericCollection(int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Отрицательный размер коллекции недопустим.");
            }
            else
            {
                _contents = new T[count];
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Свойство только для чтения, позволяющее получать размер коллекции.
        /// </summary>
        public int Count
        {
            get
            {
                return _contents.Length;
            }
        }

        /// <summary>
        /// Свойство, позволяющее получать и задавать значение флага "Только для чтения" для коллекции.
        /// </summary>
        public bool IsReadOnly { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Метод, реализующий добавление нового элемента в коллекцию.
        /// </summary>
        public void Add(T item)
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("Коллекция доступна только для чтения.");
            }

            Array.Resize(ref _contents, Count + 1);

            _contents[Count - 1] = item;

            CollectionEvent?.Invoke(item, EventType.Added);
        }

        /// <summary>
        /// Метод, реализующий полную очистку коллекции.
        /// </summary>
        public void Clear()
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("Коллекция доступна только для чтения.");
            }

            _contents = new T[0];
        }

        /// <summary>
        /// Метод, определяющий, входит ли элемент в коллекцию.
        /// </summary>
        public bool Contains(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i].Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Метод, реализующий копирование элементов коллекции в массив Array, начиная с указанного индекса массива Array.
        /// </summary>
        /// <param name="array">Одномерный массив Array, в который копируются элементы. Массив Array должен иметь индексацию.</param>
        /// <param name="arrayIndex">Отсчитываемый от нуля индекс в массиве array, указывающий начало копирования.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new NullReferenceException("null-ссылки на массив недопустимы.");
            }

            if (arrayIndex < 0)
            {
                throw new IndexOutOfRangeException("Отрицательный индекс массива недопустим.");
            }

            if (array.Length - arrayIndex < Count)
            {
                throw new ArgumentException("Размер коллекции превышает cвободное пространство от указанного индекса массива до его конца.");
            }

            for (int i = 0; i < Count; i++)
            {
                array[i + arrayIndex] = _contents[i];
            }

        }

        /// <summary>
        /// Метод, реализующий удаление первого вхождения указанного члена из коллекции.
        /// </summary>
        public bool Remove(T item)
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("Коллекция доступна только для чтения.");
            }

            for (int i = 0; i < Count; i++)
            {
                if (_contents[i].Equals(item))
                {
                    for (int j = i; j < Count - 1; j++)
                    {
                        _contents[j] = _contents[j + 1];
                    }

                    Array.Resize(ref _contents, Count - 1);

                    CollectionEvent?.Invoke(item, EventType.Removed);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Метод, возвращающий перечислитель, который осуществляет итерацию по коллекции.
        /// </summary>
        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_contents).GetEnumerator();
        }

        /// <summary>
        /// Метод, возвращающий перечислитель, который осуществляет итерацию по коллекции.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _contents.GetEnumerator();
        }

        #endregion

        #region Indexator

        /// <summary>
        /// Индексатор коллекции.
        /// </summary>
        public T this[int index]
        {
            get
            {
                if ((index < 0 ) || (index > Count - 1))
                {
                    throw new IndexOutOfRangeException("Недопустимый индекс.");
                }

                CollectionEvent?.Invoke(_contents[index], EventType.Received);

                return _contents[index];
            }
            set
            {
                if ((index < 0) || (index > Count - 1))
                {
                    throw new IndexOutOfRangeException("Недопустимый индекс.");
                }

                CollectionEvent?.Invoke(_contents[index], EventType.Updated);

                _contents[index] = value;
            }
        }

        #endregion
    }
}
