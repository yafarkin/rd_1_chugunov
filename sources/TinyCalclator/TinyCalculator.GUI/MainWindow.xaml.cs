﻿using System;
using System.Windows;
using System.Windows.Media;

using TinyCalculator.Core;

namespace TinyCalculator.GUI
{
    public partial class MainWindow : Window
    {
        Radix radix = Radix.Binary;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalculateNewtonsRoot_Click(object sender, RoutedEventArgs e)
        {
            lblPrompt.Foreground = Brushes.Black;
            lblPrompt.Content = "Заполните поля, представленные ниже и нажмите кнопку 'Вычислить'";

            var dataIsCorrect = true;

            var number = 0d;
            var power = 0d;
            var accuracy = 0d;

            dataIsCorrect = double.TryParse(txtNumber.Text, out number);
            dataIsCorrect = double.TryParse(txtRootPower.Text, out power);
            dataIsCorrect = double.TryParse(txtAccuracy.Text, out accuracy);

            if (dataIsCorrect)
            {
                try
                {
                    var dotNetResult = Math.Pow(number, 1 / power);
                    var libraryResult = TinyMathLibrary.NewtonsRoot(number, power, accuracy);

                    txtNewtonsRootResult.Text = libraryResult.ToString();
                    txtDotNetResult.Text = dotNetResult.ToString();
                }
                catch (ArgumentException ex)
                {
                    lblPrompt.Foreground = Brushes.Red;
                    lblPrompt.Content = ex.ParamName;
                }
            }
            else
            {
                lblPrompt.Foreground = Brushes.Red;
                lblPrompt.Content = "Недопустимый ввод.";
            }
        }

        private void btnClearNewtonsRootResults_Click(object sender, RoutedEventArgs e)
        {
            lblPrompt.Foreground = Brushes.Black;
            lblPrompt.Content = "Заполните поля, представленные ниже и нажмите кнопку 'Вычислить'";

            txtNumber.Text = string.Empty;
            txtRootPower.Text = string.Empty;
            txtAccuracy.Text = string.Empty;
            txtDotNetResult.Text = string.Empty;
            txtNewtonsRootResult.Text = string.Empty;

            txtNumber.Focus();

        }


        private void btnConvert_Click(object sender, RoutedEventArgs e)
        {
            lblConvertingPrompt.Foreground = Brushes.Black;
            lblConvertingPrompt.Content = "Укажите число, систему счисления и нажмите кнопку 'Перевести'";

            txtLibraryConvertResult.Text = string.Empty;
            txtDotNetConvertResult.Text = string.Empty;

            var numberToConvert = 0;

            var dataIsCorrect = int.TryParse(txtNumberToConvert.Text, out numberToConvert);

            if (dataIsCorrect)
            {
                var libraryConvertingResult = Convert.ToString(Math.Abs(numberToConvert), (int)radix);
                var dotNetConvertingResult = TinyMathLibrary.ConvertToNumberSystem(Math.Abs(numberToConvert), radix);

                txtLibraryConvertResult.Text = libraryConvertingResult.ToString();
                txtDotNetConvertResult.Text = dotNetConvertingResult.ToString();
            }
            else
            {
                lblConvertingPrompt.Foreground = Brushes.Red;
                lblConvertingPrompt.Content = "Недопустимый ввод.";

                txtNumberToConvert.Focus();
            }
        }

        private void rbBinary_Checked(object sender, RoutedEventArgs e)
        {
            radix = Radix.Binary;
        }

        private void rbOctal_Checked(object sender, RoutedEventArgs e)
        {
            radix = Radix.Octal;
        }

        private void rbHexadecimal_Checked(object sender, RoutedEventArgs e)
        {
            radix = Radix.Hexadecimal;
        }


        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите завершить работу с приложением?", "Подтвреждение выхода", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnConvert_Copy_Click(object sender, RoutedEventArgs e)
        {
            lblConvertingPrompt.Foreground = Brushes.Black;
            lblConvertingPrompt.Content = "Укажите число, систему счисления и нажмите кнопку 'Перевести'";

            txtNumberToConvert.Text = string.Empty;
            txtLibraryConvertResult.Text = string.Empty;
            txtDotNetConvertResult.Text = string.Empty;

            radix = Radix.Binary;

            rbBinary.IsChecked = true;

            txtNumberToConvert.Focus();
        }
    }
}
