﻿using System;

namespace TinyCalculator.Core
{
    /// <summary>
    /// Перечисление систем счисления.
    /// </summary>
    public enum Radix
    {
        /// <summary>
        /// Двоичная система счисления.
        /// </summary>
        Binary = 2,

        /// <summary>
        /// Восьмеричная система счисления.
        /// </summary>
        Octal = 8,

        /// <summary>
        /// Шестнадцатиричная система счисления.
        /// </summary>
        Hexadecimal = 16
    }
}
