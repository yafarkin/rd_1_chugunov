﻿using System;

namespace TinyCalculator.Core
{
    /// <summary>
    /// Класс, реализующий библиотеку с минимальным набором математических операций.
    /// </summary>
    public static class TinyMathLibrary
    {
        /// <summary>
        /// Метод, реализующий вычисление корня n-ой степени с заданной точностью методом Ньютона.
        /// </summary>
        /// <param name="A">Число, из которого необходимо вычислить корень.</param>
        /// <param name="n">Степень извлечения корня.</param>
        /// <param name="epsilon">Точность извлечения корня.</param>
        public static double NewtonsRoot(double A, double n, double epsilon = 0.0001)
        {
            // Невозможно извлечь корень 0 степени из числа.
            if (n == 0)
            {
                throw new ArgumentException("Некорректное значение степени корня.", "Степень корня не может быть нулевой.");
            }

            // Невозможно извлечь корень чётной степени из отрицательного числа.
            if (A < 0 && n % 2 == 0)
            {
                throw new ArgumentException("Некорректное значение степени корня.", "Степень корня для отрицательного числа не может быть чётной.");
            }

            // Точность вычислений не может превышать 1 и быть меньше 0.
            if (epsilon > 1 || epsilon <= 0)
            {
                throw new ArgumentOutOfRangeException("Некорректное значение точности вычислений.", "Точность вычислений должна находиться в интервале (0..1].");
            }

            // Корень любой степени из 0 определенно 0.
            if (A == 0)
            {
                return 0;
            }

            // Начальное приближение.
            var x0 = Math.Sqrt(A);

            // Вычисление первого значения.
            var xk = (1 / n) * ((n - 1) * x0 + (A / Math.Pow(x0, n - 1)));
            
            // Итерационное вычисление последующих приближений, значений и погрешностей.
            // Вычисления продолжаются, пока точность вычислений ниже заданной.
            while (Math.Abs((xk - x0)) > epsilon)
            {
                x0 = xk;
                xk = (1 / n) * ((n - 1) * x0 + (A / Math.Pow(x0, n - 1)));
            }

            return xk;
        }

        /// <summary>
        /// Метод, реализующий перевод числа из десятичной системы счисления 
        /// в другую систему счисления .
        /// </summary>
        /// <param name="number">Число, которое требуется перевести.</param>
        /// <param name="radix">Система счисления (2, 8, 10).</param>
        public static string ConvertToNumberSystem(int number, Radix radix)
        {
            var currentBit = 32;
            var decimalBasis = 10;
            var basis = (int)radix;
            var bits = new byte[32];
            var hexadecimalCharset = new[] { 'a', 'b', 'c', 'd', 'e', 'f' };
            var result = string.Empty;

            for ( ; number > 0; number /= basis)
            {
                bits[--currentBit] = (byte)(number % basis);
            }

            for (int i = 0; i < bits.Length; i++)
            {
                result += bits[i] >= decimalBasis ? hexadecimalCharset[(int)(bits[i] % decimalBasis)].ToString()
                                                   : bits[i].ToString();
            }

            result = result.TrimStart(new char[] { '0' });

            return result; 
        }
    }
}
