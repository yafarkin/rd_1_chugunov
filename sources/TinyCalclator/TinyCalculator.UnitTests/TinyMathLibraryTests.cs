﻿using System;
using TinyCalculator.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TinyCalculator.UnitTests
{
    [TestClass]
    public class TinyMathLibraryTests
    {
        /// <summary>
        /// Тестовый метод, реализующий проверку корректности
        /// вычисления корня заданной степени из числа с указанной точностью 
        /// методом Ньютона.
        /// </summary>
        [TestMethod]
        public void NewtonsRootIsCalculatingCorrect()
        {
            var number = 25d;
            var rootPower = 3.0d;
            var epsilon = 0.01d;

            var expectedResult = Math.Pow(number, 1.0 / rootPower);  
            var actualResult = TinyMathLibrary.NewtonsRoot(number, rootPower, epsilon);

            Assert.AreEqual(expectedResult, actualResult, epsilon);

        }
        
        /// <summary>
        /// Тестовый метод, реализующий проверку выброса исключения ArgumentException
        /// при заданной нулевой степени извлечения корня.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewtonsRootThrowsExceptionWhenPowerIsEqualToZero()
        {
            var number = 4d;
            var rootPower = 0d;

            var result = TinyMathLibrary.NewtonsRoot(number, rootPower);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку выброса исключения ArgumentException
        /// при заданной положительной степени корня для отрицательного числа.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewtonsRootThrowsExceptionWhenPowerIsEvenAndNumberIsNegative()
        {
            var number = -6d;
            var rootPower = 2d;

            var result = TinyMathLibrary.NewtonsRoot(number, rootPower);
        }

        /// <summary>
        /// Тестовый метод, реализующий проверку выброса исключения ArgumentOutOfRangeException
        /// при некорректно указанной точности вычисления корня методом Ньютона.
        /// Некорректная точность - точность, не принадлежащая интервалу (0..1].
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void NewtonsRootThrowsExceptionWhenEpsilonIsIncorrect()
        {
            var number = -6d;
            var rootPower = 3d;
            var epsilon = 0d;

            var actualResult = TinyMathLibrary.NewtonsRoot(number, rootPower, epsilon);
        }

        /// <summary>
        /// Тестовый метод, реализующий провекру корректности перевода числа в заданную систему счисления.
        /// </summary>
        [TestMethod]
        public void ConvertsToAnotherNumberSystemCorrect()
        {
            var numberToConvert = 598;

            var expectedResult = System.Convert.ToString(numberToConvert,16);
            var actualResult = TinyMathLibrary.ConvertToNumberSystem(numberToConvert, Radix.Hexadecimal);

            Assert.AreEqual(expectedResult, actualResult);
        }

    }
}
